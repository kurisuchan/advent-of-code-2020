package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day23"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("CrabGame: %s\n", day23.CrabGame(input, 100))
	fmt.Printf("MillionCupCrab: %d\n", day23.MillionCupCrab(input))
}
