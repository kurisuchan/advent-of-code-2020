package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day03"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("CountTrees: %d\n", day03.CountTrees(input))
	fmt.Printf("MinimizeArborealStop: %d\n", day03.MinimizeArborealStop(input))
}
