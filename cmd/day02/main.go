package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day02"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("PasswordPolicy: %d\n", day02.PasswordPolicy(input))
	fmt.Printf("TobogganPolicy: %d\n", day02.TobogganPolicy(input))
}
