package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day01"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	fmt.Printf("ExpenseReport: %d\n", day01.ExpenseReport(input))
	fmt.Printf("TripleExpenseReport: %d\n", day01.TripleExpenseReport(input))
}
