package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day06"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringSpaceGroupsFromFile("input.txt")

	fmt.Printf("CountQuestions: %d\n", day06.CountQuestions(input))
	fmt.Printf("CountCommonQuestions: %d\n", day06.CountCommonQuestions(input))
}
