package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day21"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("SafeIngredients: %d\n", day21.SafeIngredients(input))
	fmt.Printf("CanonicalDangerousIngredientList: %s\n", day21.CanonicalDangerousIngredientList(input))
}
