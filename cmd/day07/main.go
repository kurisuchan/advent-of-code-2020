package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day07"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	bags := day07.GetBags(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("WhoCanGold: %d\n", day07.WhoCanGold(bags))
	fmt.Printf("HowManyBags: %d\n", day07.HowManyBags(bags))
}
