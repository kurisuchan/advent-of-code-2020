package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day24"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("TileFlipper: %d\n", day24.TileFlipper(input))
	fmt.Printf("ArtExhibit: %d\n", day24.ArtExhibit(input))
}
