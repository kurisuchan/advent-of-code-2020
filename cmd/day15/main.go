package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day15"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("MemoryGame: %d\n", day15.MemoryGame(input))
	fmt.Printf("GameChallenge: %d\n", day15.GameChallenge(input))
}
