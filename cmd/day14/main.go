package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day14"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("MaskValues: %d\n", day14.MaskValues(input))
	fmt.Printf("FloatingAddresses: %d\n", day14.FloatingAddresses(input))
}
