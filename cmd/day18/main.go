package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day18"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("LTRMath: %d\n", day18.LTRMath(input))
	fmt.Printf("AdvancedMath: %d\n", day18.AdvancedMath(input))
}
