package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day11"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("AdjacentSeating: %d\n", day11.AdjacentSeating(input))
	fmt.Printf("VisibleSeating: %d\n", day11.VisibleSeating(input))
}
