package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day20"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("TileAssembly: %d\n", day20.TileAssembly(input))
	fmt.Printf("RoughSeas: %d\n", day20.RoughSeas(input))
}
