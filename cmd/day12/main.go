package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day12"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("EvasiveAction: %d\n", day12.EvasiveAction(input))
	fmt.Printf("FollowWaypoint: %d\n", day12.FollowWaypoint(input))
}
