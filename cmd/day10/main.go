package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day10"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	fmt.Printf("JoltDifferences: %d\n", day10.JoltDifferences(input))
	fmt.Printf("DistinctWays: %d\n", day10.DistinctWays(input))
}
