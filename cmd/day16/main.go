package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day16"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("TicketScanningErrorRate: %d\n", day16.TicketScanningErrorRate(input))
	fmt.Printf("MyTicket: %d\n", day16.MyTicket(input, "departure"))
}
