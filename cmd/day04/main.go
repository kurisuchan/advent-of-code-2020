package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day04"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("Passports with all fields: %d\n", len(day04.Parse(input, day04.CheckMissing)))
	fmt.Printf("Passports with valid data: %d\n", len(day04.Parse(input, day04.CheckData)))
}
