package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day25"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	fmt.Printf("EncryptionKey: %d\n", day25.EncryptionKey(input))
}
