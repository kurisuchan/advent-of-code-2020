package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day17"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("PocketCube: %d\n", day17.PocketCube(input))
	fmt.Printf("PocketHypercube: %d\n", day17.PocketHypercube(input))
}
