package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day13"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("NextBus: %d\n", day13.NextBus(input))
	fmt.Printf("ScheduleContest: %d\n", day13.ScheduleContest(input))
}
