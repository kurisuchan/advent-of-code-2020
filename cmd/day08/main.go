package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day08"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.NoEmptyStrings(utils.ReadStringLinesFromFile("input.txt"))

	fmt.Printf("InfiniteLoop: %d\n", day08.InfiniteLoop(input))
	fmt.Printf("CleanExit: %d\n", day08.CleanExit(input))
}
