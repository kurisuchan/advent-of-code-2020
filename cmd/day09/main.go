package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day09"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	weakness := day09.InvalidNumber(input, 25)
	fmt.Printf("InvalidNumber: %d\n", weakness)
	fmt.Printf("EncryptionWeakness: %d\n", day09.EncryptionWeakness(input, weakness))
}
