package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day05"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringLinesFromFile("input.txt")

	fmt.Printf("HighestSeatID: %d\n", day05.HighestSeatID(input))
	fmt.Printf("MissingSeatID: %d\n", day05.MissingSeatID(input))
}
