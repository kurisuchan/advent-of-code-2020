package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day22"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("Combat: %d\n", day22.Combat(input))
	fmt.Printf("RecursiveCombat: %d\n", day22.RecursiveCombat(input))
}
