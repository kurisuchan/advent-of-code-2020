This repository is a collection of my solutions for the [Advent of Code 2020](http://adventofcode.com/2020) calendar.

Puzzle                                                    | Silver                | Gold
--------------------------------------------------------- | --------------------- | ---------------------
[Day 1: Report Repair](pkg/day01)                         | `00:13:11`  (` 2396`) | `00:15:24`  (` 2173`)
[Day 2: Password Philosophy](pkg/day02)                   | `00:16:30`  (` 3756`) | `00:22:21`  (` 3422`)
[Day 3: Toboggan Trajectory](pkg/day03)                   | `00:34:02`  (` 6878`) | `00:41:47`  (` 6185`)
[Day 4: Passport Processing](pkg/day04)                   | `00:18:41`  (` 3726`) | `00:39:59`  (` 2268`)
[Day 5: Binary Boarding](pkg/day05)                       | `02:24:24`  (`12185`) | `02:34:25`  (`11649`) [^1]
[Day 6: Custom Customs](pkg/day06)                        | `00:09:59`  (` 3682`) | `00:15:42`  (` 2690`)
[Day 7: Handy Haversacks](pkg/day07)                      | `00:26:52`  (` 2033`) | `00:33:34`  (` 1273`)
[Day 8: Handheld Halting](pkg/day08)                      | `00:11:52`  (` 2951`) | `00:29:21`  (` 3027`)
[Day 9: Encoding Error](pkg/day09)                        | `00:51:41`  (` 8792`) | `01:06:24`  (` 7923`)
[Day 10: Adapter Array](pkg/day10)                        | `00:09:56`  (` 2256`) | `01:57:12`  (` 5816`)
[Day 11: Seating System](pkg/day11)                       | `00:16:27`  (`  703`) | `00:31:51`  (`  989`)
[Day 12: Rain Risk](pkg/day12)                            | `00:18:29`  (` 2503`) | `00:28:26`  (` 1418`)
[Day 13: Shuttle Search](pkg/day13)                       | `00:08:44`  (` 1292`) | `00:44:42`  (`  961`)
[Day 14: Docking Data](pkg/day14)                         | `00:22:47`  (` 1929`) | `00:49:29`  (` 1733`)
[Day 15: Rambunctious Recitation](pkg/day15)              | `00:46:38`  (` 4907`) | `00:47:29`  (` 3289`)
[Day 16: Ticket Translation](pkg/day16)                   | `00:11:30`  (`  581`) | `01:07:44`  (` 2245`)
[Day 17: Conway Cubes](pkg/day17)                         | `00:30:54`  (` 1212`) | `00:40:52`  (` 1298`)
[Day 18: Operation Order](pkg/day18)                      | `01:04:55`  (` 3447`) | `01:06:33`  (` 2103`)
[Day 19: Monster Messages](pkg/day19)                     | `00:53:15`  (` 1543`) | `01:05:05`  (`  749`)
[Day 20: Jurassic Jigsaw](pkg/day20)                      | `01:45:10`  (` 2353`) | `03:01:45`  (`  851`)
[Day 21: Allergen Assessment](pkg/day21)                  | `00:40:16`  (` 1609`) | `00:53:58`  (` 1681`)
[Day 22: Crab Combat](pkg/day22)                          | `00:16:25`  (` 2199`) | `01:02:27`  (` 1771`)
[Day 23: Crab Cups](pkg/day23)                            | `00:28:20`  (`  925`) | `00:53:24`  (`  299`)
[Day 24: Lobby Layout](pkg/day24)                         | `00:29:38`  (` 1753`) | `01:11:25`  (` 2072`)
[Day 25: Combo Breaker](pkg/day25)                        | `00:30:09`  (` 2249`) | `00:31:04`  (` 1902`)


[^1]: Overslept by exactly two hours, whoops...
