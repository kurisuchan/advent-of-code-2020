package day10_test

import (
	"testing"

	"advent-of-code-2020/pkg/day10"
)

func TestJoltDifferences(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			16,
			10,
			15,
			5,
			1,
			11,
			7,
			19,
			6,
			12,
			4,
		}, 7 * 5},
		{[]int{
			28,
			33,
			18,
			42,
			31,
			14,
			46,
			20,
			48,
			47,
			24,
			23,
			49,
			45,
			19,
			38,
			39,
			11,
			1,
			32,
			25,
			35,
			8,
			17,
			7,
			9,
			4,
			2,
			34,
			10,
			3,
		}, 22 * 10},
	}

	for _, test := range tests {
		actual := day10.JoltDifferences(test.in)
		if actual != test.out {
			t.Errorf("JoltDifferences(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestDistinctWays(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			16,
			10,
			15,
			5,
			1,
			11,
			7,
			19,
			6,
			12,
			4,
		}, 8},
		{[]int{
			28,
			33,
			18,
			42,
			31,
			14,
			46,
			20,
			48,
			47,
			24,
			23,
			49,
			45,
			19,
			38,
			39,
			11,
			1,
			32,
			25,
			35,
			8,
			17,
			7,
			9,
			4,
			2,
			34,
			10,
			3,
		}, 19208},
	}

	for _, test := range tests {
		actual := day10.DistinctWays(test.in)
		if actual != test.out {
			t.Errorf("DistinctWays(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
