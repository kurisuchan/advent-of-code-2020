package day10

import (
	"sort"
)

func JoltDifferences(outputs []int) int {
	adapters := make([]int, len(outputs)+1)
	copy(adapters, outputs)

	sort.Ints(adapters)

	builtin := adapters[len(adapters)-1] + 3
	adapters = append(adapters, builtin)

	var outlet, diff1, diff3 int

	for _, adapter := range adapters {
		switch adapter - outlet {
		case 3:
			diff3++
		case 1:
			diff1++
		}

		outlet = adapter
	}

	return diff1 * diff3
}

func DistinctWays(outputs []int) int {
	sockets := append([]int{0}, outputs...)

	sort.Sort(sort.Reverse(sort.IntSlice(sockets)))

	builtin := sockets[0] + 3
	paths := map[int]int{
		builtin: 1,
	}

	for _, socket := range sockets {
		paths[socket] = paths[socket+1] + paths[socket+2] + paths[socket+3]
	}

	return paths[0]
}
