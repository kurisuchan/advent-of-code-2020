package day13

import (
	"math/big"
	"strings"

	"advent-of-code-2020/pkg/utils"
)

func NextBus(input string) int {
	lines := strings.Split(input, "\n")
	earliest := utils.MustInt(lines[0])

	var nextBus, nextWait int

	for _, num := range strings.Split(lines[1], ",") {
		if num != "x" {
			bus := utils.MustInt(num)
			wait := bus - (earliest % bus)

			if wait < nextWait || nextWait == 0 {
				nextWait = wait
				nextBus = bus
			}
		}
	}

	return nextBus * nextWait
}

func ScheduleContest(input string) int64 {
	lines := strings.Split(input, "\n")
	buses := strings.Split(lines[1], ",")

	var n, a []*big.Int

	for offset, bus := range buses {
		if bus != "x" {
			n = append(n, big.NewInt(int64(utils.MustInt(bus))))
			a = append(a, big.NewInt(int64(0-offset)))
		}
	}

	res, err := utils.CRT(a, n)
	if err != nil {
		panic(err)
	}

	return res.Int64()
}
