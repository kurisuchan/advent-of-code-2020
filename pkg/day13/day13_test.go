package day13_test

import (
	"testing"

	"advent-of-code-2020/pkg/day13"
)

func TestNextBus(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"939\n7,13,x,x,59,x,31,19", 295},
	}

	for _, test := range tests {
		actual := day13.NextBus(test.in)
		if actual != test.out {
			t.Errorf("NextBus(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestScheduleContest(t *testing.T) {
	tests := []struct {
		in  string
		out int64
	}{
		{"\n7,13,x,x,59,x,31,19", 1068781},
		{"\n17,x,13,19", 3417},
		{"\n67,7,59,61", 754018},
		{"\n67,x,7,59,61", 779210},
		{"\n67,7,x,59,61", 1261476},
		{"\n1789,37,47,1889", 1202161486},
	}

	for _, test := range tests {
		actual := day13.ScheduleContest(test.in)
		if actual != test.out {
			t.Errorf("ScheduleContest(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
