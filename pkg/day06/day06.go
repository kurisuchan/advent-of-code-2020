package day06

import (
	"strings"
)

type void = struct{}
type questionForm map[uint8]void

func CountQuestions(answers [][]string) int {
	var result int

	for group := range answers {
		questions := questionForm{}

		for line := range answers[group] {
			for r := range answers[group][line] {
				questions[answers[group][line][r]] = void{}
			}
		}

		result += len(questions)
	}

	return result
}

func CountCommonQuestions(answers [][]string) int {
	var result int

	for group := range answers {
		questions := questionForm{}

		for line := range answers[group] {
			if line == 0 {
				for r := range answers[group][line] {
					questions[answers[group][line][r]] = void{}
				}
			} else {
				for k := range questions {
					if !strings.ContainsRune(answers[group][line], rune(k)) {
						delete(questions, k)
					}
				}
			}
		}

		result += len(questions)
	}

	return result
}
