package day06_test

import (
	"testing"

	"advent-of-code-2020/pkg/day06"
)

func TestCountQuestions(t *testing.T) {
	tests := []struct {
		in  [][]string
		out int
	}{
		{[][]string{{
			"abcx",
			"abcy",
			"abcz",
		}}, 6},
		{[][]string{
			{
				"abc",
			},
			{
				"a",
				"b",
				"c",
			},
			{
				"ab",
				"ac",
			},
			{
				"a",
				"a",
				"a",
				"a",
			},
			{
				"b",
			},
		}, 11},
	}

	for _, test := range tests {
		actual := day06.CountQuestions(test.in)
		if actual != test.out {
			t.Errorf("CountQuestions(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCountCommonQuestions(t *testing.T) {
	tests := []struct {
		in  [][]string
		out int
	}{
		{[][]string{{
			"abcx",
			"abcy",
			"abcz",
		}}, 3},
		{[][]string{
			{
				"abc",
			},
			{
				"a",
				"b",
				"c",
			},
			{
				"ab",
				"ac",
			},
			{
				"a",
				"a",
				"a",
				"a",
			},
			{
				"b",
			},
		}, 6},
	}

	for _, test := range tests {
		actual := day06.CountCommonQuestions(test.in)
		if actual != test.out {
			t.Errorf("CountCommonQuestions(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
