package day03

import "advent-of-code-2020/pkg/utils"

func CheckForTrees(input []string, slope utils.Coordinate2) int {
	p := utils.Coordinate2{}
	trees := 0

	for p.Y < len(input) {
		if input[p.Y][p.X] == '#' {
			trees++
		}
		p.X = (p.X + slope.X) % len(input[0])
		p.Y += slope.Y
	}

	return trees
}

func CountTrees(input []string) int {
	return CheckForTrees(input, utils.Coordinate2{X: 3, Y: 1})
}

func MinimizeArborealStop(input []string) int {
	slopes := []utils.Coordinate2{
		{X: 1, Y: 1},
		{X: 3, Y: 1},
		{X: 5, Y: 1},
		{X: 7, Y: 1},
		{X: 1, Y: 2},
	}
	result := 1

	for _, slope := range slopes {
		result *= CheckForTrees(input, slope)
	}

	return result
}
