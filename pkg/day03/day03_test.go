package day03_test

import (
	"testing"

	"advent-of-code-2020/pkg/day03"
	"advent-of-code-2020/pkg/utils"
)

func TestCountTrees(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{
			[]string{
				"..##.......",
				"#...#...#..",
				".#....#..#.",
				"..#.#...#.#",
				".#...##..#.",
				"..#.##.....",
				".#.#.#....#",
				".#........#",
				"#.##...#...",
				"#...##....#",
				".#..#...#.#",
			},
			7,
		},
	}

	for _, test := range tests {
		actual := day03.CountTrees(test.in)
		if actual != test.out {
			t.Errorf("CountTrees(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCheckForTrees(t *testing.T) {
	input := []string{
		"..##.......",
		"#...#...#..",
		".#....#..#.",
		"..#.#...#.#",
		".#...##..#.",
		"..#.##.....",
		".#.#.#....#",
		".#........#",
		"#.##...#...",
		"#...##....#",
		".#..#...#.#",
	}

	tests := []struct {
		in  utils.Coordinate2
		out int
	}{
		{
			utils.Coordinate2{X: 1, Y: 1},
			2,
		},
		{
			utils.Coordinate2{X: 3, Y: 1},
			7,
		},
		{
			utils.Coordinate2{X: 5, Y: 1},
			3,
		},
		{
			utils.Coordinate2{X: 7, Y: 1},
			4,
		},
		{
			utils.Coordinate2{X: 1, Y: 2},
			2,
		},
	}

	for _, test := range tests {
		actual := day03.CheckForTrees(input, test.in)
		if actual != test.out {
			t.Errorf("CheckForTrees(%+v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMinimizeArborealStop(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{
			[]string{
				"..##.......",
				"#...#...#..",
				".#....#..#.",
				"..#.#...#.#",
				".#...##..#.",
				"..#.##.....",
				".#.#.#....#",
				".#........#",
				"#.##...#...",
				"#...##....#",
				".#..#...#.#",
			},
			336,
		},
	}

	for _, test := range tests {
		actual := day03.MinimizeArborealStop(test.in)
		if actual != test.out {
			t.Errorf("MinimizeArborealStop(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
