package day02_test

import (
	"testing"

	"advent-of-code-2020/pkg/day02"
)

func TestParsePassword(t *testing.T) {
	tests := []struct {
		in        string
		pos1      int
		pos2      int
		character uint8
		password  string
	}{
		{
			"1-3 a: abcde",
			1, 3, 'a', "abcde",
		},
		{
			"1-3 b: cdefg",
			1, 3, 'b', "cdefg",
		},
		{
			"2-9 c: ccccccccc",
			2, 9, 'c', "ccccccccc",
		},
	}

	for _, test := range tests {
		pos1, pos2, character, password := day02.ParsePassword(test.in)
		if pos1 != test.pos1 || pos2 != test.pos2 || character != test.character || password != test.password {
			t.Errorf("ParsePassword(%q) => %d, %d, %c, %s, want %d, %d, %c, %s",
				test.in,
				pos1, pos2, character, password,
				test.pos1, test.pos2, test.character, test.password,
			)
		}
	}
}

func TestPasswordPolicy(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"1-3 a: abcde",
			"1-3 b: cdefg",
			"2-9 c: ccccccccc",
		}, 2},
	}

	for _, test := range tests {
		actual := day02.PasswordPolicy(test.in)
		if actual != test.out {
			t.Errorf("PasswordPolicy(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTobogganPolicy(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"1-3 a: abcde",
			"1-3 b: cdefg",
			"2-9 c: ccccccccc",
		}, 1},
	}

	for _, test := range tests {
		actual := day02.TobogganPolicy(test.in)
		if actual != test.out {
			t.Errorf("TobogganPolicy(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
