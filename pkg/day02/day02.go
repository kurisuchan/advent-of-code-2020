package day02

import (
	"regexp"
	"strings"

	"advent-of-code-2020/pkg/utils"
)

var passwordLine = regexp.MustCompile(`^([[:digit:]]+)-([[:digit:]]+) ([[:alpha:]]): ([[:alpha:]]+)$`)

func PasswordPolicy(passwords []string) int {
	var validPasswords int

	for _, p := range passwords {
		min, max, letter, password := ParsePassword(p)
		if min == 0 && max == 0 {
			continue
		}

		matches := strings.Count(password, string(letter))

		if matches >= min && matches <= max {
			validPasswords++
		}
	}

	return validPasswords
}

func TobogganPolicy(passwords []string) int {
	var validPasswords int

	for _, p := range passwords {
		pos1, pos2, letter, password := ParsePassword(p)
		if pos1 == 0 && pos2 == 0 {
			continue
		}
		pos1--
		pos2--

		if password[pos1] == letter && password[pos2] != letter ||
			password[pos1] != letter && password[pos2] == letter {
			validPasswords++
		}
	}

	return validPasswords
}

func ParsePassword(input string) (min int, max int, letter byte, password string) {
	match := passwordLine.FindStringSubmatch(input)
	if match != nil {
		min = utils.MustInt(match[1])
		max = utils.MustInt(match[2])
		letter = match[3][0]
		password = match[4]
	}
	return
}
