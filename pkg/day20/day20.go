package day20

import (
	"regexp"
	"strings"

	"advent-of-code-2020/pkg/utils"
)

func getTiles(input string) map[int]*Tile {
	tiles := make(map[int]*Tile)
	tileIdMatch := regexp.MustCompile(`^Tile (\d+):`)

	for _, block := range strings.Split(input, "\n\n") {
		tileId := tileIdMatch.FindStringSubmatch(block)
		if tileId == nil {
			continue
		}

		image := NewTile()

		for _, line := range strings.Split(block, "\n")[1:] {
			if line == "" {
				continue
			}

			var lineData []bool

			for _, r := range line {
				lineData = append(lineData, r == '#')
			}

			image.ImageData = append(image.ImageData, lineData)
		}

		tiles[utils.MustInt(tileId[1])] = image
	}

	return tiles
}

func align(tiles map[int]*Tile) {
	aligned := make(map[int]struct{})

	for tile := range tiles {
		if len(tiles[tile].Neighbors) != 0 {
			aligned[tile] = struct{}{}
		}
	}

	if len(aligned) == 0 {
		for tile := range tiles {
			aligned[tile] = struct{}{}
			break
		}
	}

	newMatch := true
	for newMatch {
		newMatch = false
		for tile := range aligned {
			for other := range tiles {
				if other == tile {
					continue
				}

				_, locked := aligned[other]

				if tiles[tile].HasNeighbor(tiles[other], !locked) {
					newMatch = true
					aligned[other] = struct{}{}
				}
			}
		}
	}
}

func stitch(tiles map[int]*Tile) [][]bool {
	visited := make(map[*Tile]struct{})
	var cur *Tile
	queue := make(map[*Tile]utils.Coordinate2)

	for tile := range tiles {
		cur = tiles[tile]
		visited[cur] = struct{}{}
		for n, p := range cur.Neighbors {
			queue[p] = utils.Coordinate2{}.Move(n)
		}
		break
	}

	if cur == nil {
		return nil
	}

	grid := map[utils.Coordinate2]*Tile{
		{}: cur,
	}

	var minX, minY, maxX, maxY int
	tileSize := len(cur.ImageData) - 2

	for len(queue) > 0 {
		for p, dir := range queue {
			delete(queue, p)
			visited[p] = struct{}{}

			for n, np := range p.Neighbors {
				if _, ok := visited[np]; !ok {
					queue[np] = dir.Move(n)
				}
			}

			grid[dir] = p

			if dir.X < minX {
				minX = dir.X
			}
			if dir.Y < minY {
				minY = dir.Y
			}
			if dir.X > maxX {
				maxX = dir.X
			}
			if dir.Y > maxY {
				maxY = dir.Y
			}
		}
	}

	out := make([][]bool, (maxY-minY+1)*tileSize)
	gridSize := maxX - minX + 1

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			tile := grid[utils.Coordinate2{X: x, Y: y}]

			for tileY, row := range tile.ImageData[1 : tileSize+1] {
				actualY := (y-minY)*tileSize + tileY

				if out[actualY] == nil {
					out[actualY] = make([]bool, gridSize*tileSize)
				}

				for tileX, t := range row[1 : len(row)-1] {
					actualX := (x-minX)*tileSize + tileX
					out[actualY][actualX] = t
				}
			}
		}
	}

	return out
}

var SeaMonster map[utils.Coordinate2]struct{}

func init() {
	SeaMonster = make(map[utils.Coordinate2]struct{})
	pattern := "                  # \n#    ##    ##    ###\n #  #  #  #  #  #   "
	for y, line := range strings.Split(pattern, "\n") {
		for x, r := range line {
			if r == '#' {
				SeaMonster[utils.Coordinate2{X: x, Y: y}] = struct{}{}
			}
		}
	}

}

func TileAssembly(input string) uint64 {
	tiles := getTiles(input)

	align(tiles)

	var result uint64 = 1
	for tile := range tiles {
		if len(tiles[tile].Neighbors) == 2 {
			result *= uint64(tile)
		}
	}

	return result
}

func RoughSeas(input string) int {
	tiles := getTiles(input)
	align(tiles)
	grid := stitch(tiles)

	var found bool
	for i := 0; i < 8; i++ {
		for y := range grid {
			for x := range grid[y] {
				match := true

				for m := range SeaMonster {
					if y+m.Y >= len(grid) {
						match = false
						break
					}

					if x+m.X >= len(grid[y+m.Y]) {
						match = false
						break
					}

					if !grid[y+m.Y][x+m.X] {
						match = false
						break
					}
				}

				if match {
					found = true

					for m := range SeaMonster {
						grid[y+m.Y][x+m.X] = false
					}
				}
			}
		}

		if found {
			break
		}

		if i&1 == 0 {
			grid = utils.FlipBool(grid)
		} else {
			grid = utils.RotateBool(utils.FlipBool(grid))
		}
	}

	var rough int

	for y := range grid {
		for x := range grid[y] {
			if grid[y][x] {
				rough++
			}
		}
	}

	return rough
}
