package day20_test

import (
	"testing"

	"advent-of-code-2020/pkg/day20"
)

func ExampleTile_Print() {
	tile := day20.NewTile()
	tile.ImageData = [][]bool{
		{true, true, false},
		{false, true, true},
		{true, true, true},
	}
	tile.Print()

	// Output:
	// ##.
	// .##
	// ###
}

func TestTile_HasNeighbor(t *testing.T) {
	tests := []struct {
		in     [][]bool
		modify bool
		out    bool
	}{
		{[][]bool{
			{true, true, false},
			{false, true, true},
			{true, true, true},
		}, false, false},
		{[][]bool{
			{true, false, true},
			{false, true, true},
			{true, true, true},
		}, true, true},
	}

	for i, test := range tests {
		tile := day20.NewTile()
		tile.ImageData = [][]bool{
			{true, true, false},
			{false, true, true},
			{true, true, true},
		}

		other := day20.NewTile()
		other.ImageData = test.in

		actual := tile.HasNeighbor(other, test.modify)
		if actual != test.out {
			t.Errorf("tile.HasNeighbor(%d, %t) => %t, want %t", i, test.modify, actual, test.out)
		}
	}
}

func TestTile_HasBottomNeighbor(t *testing.T) {
	tests := []struct {
		in  [][]bool
		out bool
	}{
		{[][]bool{
			{true, true, false},
			{false, true, true},
			{true, true, true},
		}, false},
		{[][]bool{
			{true, false, true},
			{false, true, true},
			{true, true, true},
		}, true},
	}

	for i, test := range tests {
		tile := day20.NewTile()
		tile.ImageData = [][]bool{
			{true, true, false},
			{false, true, true},
			{true, false, true},
		}

		other := day20.NewTile()
		other.ImageData = test.in

		actual := tile.HasBottomNeighbor(other)
		if actual != test.out {
			t.Errorf("tile.HasBottomNeighbor(%d) => %t, want %t", i, actual, test.out)
		}
	}
}

func TestTile_HasRightNeighbor(t *testing.T) {
	tests := []struct {
		in  [][]bool
		out bool
	}{
		{[][]bool{
			{false, true, false},
			{false, false, true},
			{false, true, false},
		}, false},
		{[][]bool{
			{false, false, true},
			{true, false, true},
			{false, true, true},
		}, true},
	}

	for i, test := range tests {
		tile := day20.NewTile()
		tile.ImageData = [][]bool{
			{false, true, false},
			{true, false, true},
			{false, true, false},
		}

		other := day20.NewTile()
		other.ImageData = test.in

		actual := tile.HasRightNeighbor(other)
		if actual != test.out {
			t.Errorf("tile.HasRightNeighbor(%d) => %t, want %t", i, actual, test.out)
		}
	}
}
