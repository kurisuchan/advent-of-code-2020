package day20

import (
	"fmt"

	"advent-of-code-2020/pkg/utils"
)

type Tile struct {
	ImageData [][]bool
	Neighbors map[utils.Coordinate2]*Tile
}

func NewTile() *Tile {
	return &Tile{
		Neighbors: make(map[utils.Coordinate2]*Tile),
	}
}

func (t *Tile) Flip() {
	t.ImageData = utils.FlipBool(t.ImageData)
}

func (t *Tile) Rotate() {
	t.ImageData = utils.RotateBool(t.ImageData)
}

func (t *Tile) HasRightNeighbor(o *Tile) bool {
	size := len(t.ImageData)

	for y := range t.ImageData {
		if t.ImageData[y][size-1] != o.ImageData[y][0] {
			return false
		}
	}

	return true
}

func (t *Tile) HasBottomNeighbor(o *Tile) bool {
	size := len(t.ImageData)

	for x := range t.ImageData[size-1] {
		if t.ImageData[size-1][x] != o.ImageData[0][x] {
			return false
		}
	}

	return true
}

func (t *Tile) Print() {
	for y := range t.ImageData {
		for x := range t.ImageData[y] {
			if t.ImageData[y][x] {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
}

func (t *Tile) HasNeighbor(o *Tile, modify bool) bool {
	for _, c := range utils.Coordinate2Neighbors4 {
		if _, ok := t.Neighbors[c]; ok {
			continue
		}

		for i := 0; i < 8; i++ {
			if c == utils.East && t.HasRightNeighbor(o) {
				t.Neighbors[utils.East] = o
				o.Neighbors[utils.West] = t
				return true
			}
			if c == utils.West && o.HasRightNeighbor(t) {
				t.Neighbors[utils.West] = o
				o.Neighbors[utils.East] = t
				return true
			}
			if c == utils.South && t.HasBottomNeighbor(o) {
				t.Neighbors[utils.South] = o
				o.Neighbors[utils.North] = t
				return true
			}
			if c == utils.North && o.HasBottomNeighbor(t) {
				t.Neighbors[utils.North] = o
				o.Neighbors[utils.South] = t
				return true
			}

			if !modify {
				break
			}

			if i&1 == 0 {
				o.Flip()
			} else {
				o.Flip()
				o.Rotate()
			}
		}
	}

	return false
}
