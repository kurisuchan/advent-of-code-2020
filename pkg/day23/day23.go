package day23

import (
	"container/ring"
	"fmt"
	"strings"
	"unicode"

	"advent-of-code-2020/pkg/utils"
)

func trim(r rune) bool {
	return !unicode.IsDigit(r)
}

func game(cup *ring.Ring, moves int) *ring.Ring {
	cupLen := cup.Len()
	var max int

	// build shortcut to destination so we don't have to search the ring
	cups := make(map[int]*ring.Ring, cupLen)
	for i := 0; i < cupLen; i++ {
		if cup.Value.(int) > max {
			max = cup.Value.(int)
		}

		cups[cup.Value.(int)] = cup
		cup = cup.Next()
	}

	// game
	for i := 0; i < moves; i++ {
		// pick up 3
		removed := cup.Unlink(3)

		// find destination
		destination := cup.Value.(int)

		for {
			destination -= 1
			if destination < 1 {
				destination = max
			}

			var inRemoved bool

			removed.Do(func(v interface{}) {
				if v.(int) == destination {
					inRemoved = true
				}
			})

			if !inRemoved {
				break
			}
		}

		// place removed
		cups[destination].Link(removed)

		// move clockwise
		cup = cup.Next()
	}

	return cups[1]
}

func CrabGame(input string, moves int) string {
	input = strings.TrimFunc(input, trim)
	cup := ring.New(len(input))

	for _, r := range input {
		cup.Value = utils.MustInt(string(r))
		cup = cup.Next()
	}

	cup = game(cup, moves)

	var out strings.Builder

	cup.Do(func(v interface{}) {
		if v.(int) != 1 {
			_, _ = fmt.Fprintf(&out, "%d", v.(int))
		}
	})

	return out.String()
}

func MillionCupCrab(input string) int {
	input = strings.TrimFunc(input, trim)
	cup := ring.New(1_000_000)

	for _, r := range input {
		cup.Value = utils.MustInt(string(r))
		cup = cup.Next()
	}

	for i := len(input) + 1; i <= 1_000_000; i++ {
		cup.Value = i
		cup = cup.Next()
	}

	cup = game(cup, 10_000_000)

	return cup.Next().Value.(int) * cup.Next().Next().Value.(int)
}
