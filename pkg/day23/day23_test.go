package day23_test

import (
	"testing"

	"advent-of-code-2020/pkg/day23"
)

func TestCrabGame(t *testing.T) {
	tests := []struct {
		in    string
		moves int
		out   string
	}{
		{"389125467", 10, "92658374"},
		{"389125467", 100, "67384529"},
	}

	for _, test := range tests {
		actual := day23.CrabGame(test.in, test.moves)
		if actual != test.out {
			t.Errorf("CrabGame(%q, %d) => %s, want %s", test.in, test.moves, actual, test.out)
		}
	}
}

func TestMillionCupCrab(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"389125467", 149245887792},
	}

	for _, test := range tests {
		actual := day23.MillionCupCrab(test.in)
		if actual != test.out {
			t.Errorf("MillionCupCrab(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
