package day09_test

import (
	"testing"

	"advent-of-code-2020/pkg/day09"
)

func TestInvalidNumber(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			35,
			20,
			15,
			25,
			47,
			40,
			62,
			55,
			65,
			95,
			102,
			117,
			150,
			182,
			127,
			219,
			299,
			277,
			309,
			576,
		}, 127},
	}

	for _, test := range tests {
		actual := day09.InvalidNumber(test.in, 5)
		if actual != test.out {
			t.Errorf("InvalidNumber(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestEncryptionWeakness(t *testing.T) {
	tests := []struct {
		in     []int
		target int
		out    int
	}{
		{[]int{
			35,
			20,
			15,
			25,
			47,
			40,
			62,
			55,
			65,
			95,
			102,
			117,
			150,
			182,
			127,
			219,
			299,
			277,
			309,
			576,
		}, 127,
			62},
	}

	for _, test := range tests {
		actual := day09.EncryptionWeakness(test.in, test.target)
		if actual != test.out {
			t.Errorf("EncryptionWeakness(%v, %d) => %d, want %d", test.in, test.target, actual, test.out)
		}
	}
}
