package day09

import (
	"sort"
)

type void struct{}

func InvalidNumber(input []int, preamble int) int {
	numbers := make(map[int]map[int]void)

	for i := 0; i < preamble; i++ {
		numbers[i] = make(map[int]void)

		for j := 0; j < preamble; j++ {
			if input[i] == input[j] {
				continue
			}

			numbers[i][input[i]+input[j]] = void{}
		}
	}

	for i := preamble; i < len(input); i++ {
		newNumbers := make(map[int]void)
		found := false

		for j := i - preamble; j < i; j++ {
			newNumbers[input[i]+input[j]] = void{}

			if _, ok := numbers[j][input[i]]; ok {
				found = true
			}
		}

		if !found {
			return input[i]
		}

		delete(numbers, i-preamble)
		numbers[i] = newNumbers
	}

	return -1
}

func EncryptionWeakness(input []int, target int) int {
	for i := 0; i < len(input); i++ {
		sum := input[i]

		for j := i + 1; j < len(input) && sum < target; j++ {
			if sum+input[j] == target {
				sort.Ints(input[i : j+1])
				return input[i] + input[j]
			}

			sum += input[j]
		}
	}

	return -1
}
