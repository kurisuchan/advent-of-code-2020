package day15_test

import (
	"testing"

	"advent-of-code-2020/pkg/day15"
)

func TestPlay(t *testing.T) {
	tests := []struct {
		in   string
		iter int
		out  int
	}{
		{"0,3,6", 4, 0},
		{"0,3,6", 5, 3},
		{"0,3,6", 6, 3},
		{"0,3,6", 7, 1},
		{"0,3,6", 8, 0},
		{"0,3,6", 9, 4},
		{"0,3,6", 10, 0},
	}

	for _, test := range tests {
		actual := day15.Play(test.in, test.iter)
		if actual != test.out {
			t.Errorf("Play(%q, %d) => %d, want %d", test.in, test.iter, actual, test.out)
		}
	}
}

func TestMemoryGame(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"0,3,6", 436},
		{"1,3,2", 1},
		{"2,1,3", 10},
		{"1,2,3", 27},
		{"2,3,1", 78},
		{"3,2,1", 438},
		{"3,1,2", 1836},
	}

	for _, test := range tests {
		actual := day15.MemoryGame(test.in)
		if actual != test.out {
			t.Errorf("MemoryGame(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestGameChallenge(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"0,3,6", 175594},
		{"1,3,2", 2578},
		{"2,1,3", 3544142},
		{"1,2,3", 261214},
		{"2,3,1", 6895259},
		{"3,2,1", 18},
		{"3,1,2", 362},
	}

	if testing.Short() {
		t.SkipNow()
	}

	for _, test := range tests {
		actual := day15.GameChallenge(test.in)
		if actual != test.out {
			t.Errorf("GameChallenge(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
