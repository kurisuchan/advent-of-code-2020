package day15

import (
	"strings"

	"advent-of-code-2020/pkg/utils"
)

func Play(input string, iterations int) int {
	spoken := make(map[int]int)
	var turn, last int
	
	for _, n := range strings.Split(strings.TrimSpace(input), ",") {
		if turn > 0 {
			spoken[last] = turn
		}

		last = utils.MustInt(n)
		turn++
	}

	for turn < iterations {
		if t, ok := spoken[last]; ok {
			spoken[last] = turn
			last = turn - t
		} else {
			spoken[last] = turn
			last = 0
		}

		turn++
	}

	return last
}

func MemoryGame(input string) int {
	return Play(input, 2020)
}

func GameChallenge(input string) int {
	return Play(input, 30000000)
}
