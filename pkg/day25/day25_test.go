package day25_test

import (
	"testing"

	"advent-of-code-2020/pkg/day25"
)

func TestEncryptionKey(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			5764801,
			17807724,
		}, 14897079},
	}

	for _, test := range tests {
		actual := day25.EncryptionKey(test.in)
		if actual != test.out {
			t.Errorf("EncryptionKey(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
