package day25

func findKey(target int) int {
	value := 1
	loops := 1

	for {
		value = (value * 7) % 20201227

		if value == target {
			return loops
		}

		loops++
	}
}

func transform(loopSize, subjectNumber int) int {
	value := 1

	for i := 0; i < loopSize; i++ {
		value = (value * subjectNumber) % 20201227
	}

	return value
}

func EncryptionKey(input []int) int {
	result := make(chan int, 0)

	for i := 0; i < len(input); i++ {
		go func(i int) {
			loopSize := findKey(input[i])
			result <- transform(loopSize, input[(1+i)%len(input)])
		}(i)
	}

	return <-result
}
