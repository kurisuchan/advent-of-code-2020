package day04

import (
	"regexp"
	"strconv"
	"strings"
)

type Passport struct {
	Byr string
	Iyr string
	Eyr string
	Hgt string
	Hcl string
	Ecl string
	Pid string
	Cid string
}

type ParserLevel int

const (
	CheckEmpty ParserLevel = iota
	CheckMissing
	CheckData
)

var (
	hclPattern = regexp.MustCompile("^#[0-9a-f]{6}$")
	eclPattern = regexp.MustCompile("^(?:amb|blu|brn|gry|grn|hzl|oth)$")
	pidPattern = regexp.MustCompile("^[0-9]{9}$")
)

func numberInRange(input string, min, max int) bool {
	if num, err := strconv.Atoi(input); err == nil {
		return min <= num && num <= max
	}
	return false
}

func validHeight(input, suffix string, min, max int) bool {
	return strings.HasSuffix(input, suffix) && numberInRange(strings.TrimSuffix(input, suffix), min, max)
}

func (p Passport) Valid(level ParserLevel) bool {
	switch level {
	case CheckMissing:
		return p.Byr != "" && p.Iyr != "" && p.Eyr != "" && p.Hgt != "" && p.Hcl != "" && p.Ecl != "" && p.Pid != ""
	case CheckData:
		return numberInRange(p.Byr, 1920, 2002) &&
			numberInRange(p.Iyr, 2010, 2020) &&
			numberInRange(p.Eyr, 2020, 2030) &&
			(validHeight(p.Hgt, "cm", 150, 193) || validHeight(p.Hgt, "in", 59, 76)) &&
			hclPattern.MatchString(p.Hcl) &&
			eclPattern.MatchString(p.Ecl) &&
			pidPattern.MatchString(p.Pid)
	default:
		return p != Passport{}
	}
}

func Parse(input []string, level ParserLevel) []Passport {
	var passports []Passport
	var passport Passport
	for _, line := range input {
		if line == "" {
			if passport.Valid(level) {
				passports = append(passports, passport)
			}
			passport = Passport{}
			continue
		}

		for _, field := range strings.Fields(line) {
			fields := strings.SplitN(field, ":", 2)
			switch fields[0] {
			case "byr":
				passport.Byr = fields[1]
			case "iyr":
				passport.Iyr = fields[1]
			case "eyr":
				passport.Eyr = fields[1]
			case "hgt":
				passport.Hgt = fields[1]
			case "hcl":
				passport.Hcl = fields[1]
			case "ecl":
				passport.Ecl = fields[1]
			case "pid":
				passport.Pid = fields[1]
			case "cid":
				passport.Cid = fields[1]
			}
		}
	}

	if passport.Valid(level) {
		passports = append(passports, passport)
	}

	return passports
}
