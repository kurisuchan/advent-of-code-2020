package day16_test

import (
	"testing"

	"advent-of-code-2020/pkg/day16"
)

func TestTicketScanningErrorRate(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"class: 1-3 or 5-7\nrow: 6-11 or 33-44\nseat: 13-40 or 45-50\n\nyour ticket:\n7,1,14\n\nnearby tickets:\n7,3,47\n40,4,50\n55,2,20\n38,6,12", 71},
	}

	for _, test := range tests {
		actual := day16.TicketScanningErrorRate(test.in)
		if actual != test.out {
			t.Errorf("TicketScanningErrorRate(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMyTicket(t *testing.T) {
	tests := []struct {
		in     string
		prefix string
		out    int
	}{
		{"class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9", "", 12 * 11 * 13},
		{"class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9", "class", 12},
		{"class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9", "row", 11},
		{"class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9", "seat", 13},
	}

	for _, test := range tests {
		actual := day16.MyTicket(test.in, test.prefix)
		if actual != test.out {
			t.Errorf("MyTicket(%q, %q) => %d, want %d", test.in, test.prefix, actual, test.out)
		}
	}
}
