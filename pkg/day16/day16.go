package day16

import (
	"regexp"
	"strings"

	"advent-of-code-2020/pkg/utils"
)

var (
	rangeMatch  = regexp.MustCompile(`(\d+)-(\d+)`)
	numberMatch = regexp.MustCompile(`(\d+)`)
	inputMatch  = regexp.MustCompile(`(.*?): (\d+)-(\d+) or (\d+)-(\d+)`)
)

func TicketScanningErrorRate(input string) int {
	blocks := strings.Split(input, "\n\n")
	var valid [][2]int

	for _, match := range rangeMatch.FindAllStringSubmatch(blocks[0], -1) {
		valid = append(valid, [2]int{utils.MustInt(match[1]), utils.MustInt(match[2])})
	}

	var errorRate int

	for _, match := range numberMatch.FindAllStringSubmatch(blocks[2], -1) {
		invalid := true
		number := utils.MustInt(match[1])

		for i := range valid {
			if number >= valid[i][0] && number <= valid[i][1] {
				invalid = false
				break
			}
		}

		if invalid {
			errorRate += number
		}
	}

	return errorRate
}

func MyTicket(input, prefix string) int {
	blocks := strings.Split(input, "\n\n")
	definitions := make(map[string][2][2]int)
	invalidPositions := make(map[string]map[int]struct{})

	// collect labels and ranges
	for _, match := range inputMatch.FindAllStringSubmatch(blocks[0], -1) {
		definitions[match[1]] = [2][2]int{
			{
				utils.MustInt(match[2]),
				utils.MustInt(match[3]),
			},
			{
				utils.MustInt(match[4]),
				utils.MustInt(match[5]),
			},
		}
		invalidPositions[match[1]] = make(map[int]struct{})
	}

lines:
	for _, line := range strings.Split(blocks[2], "\n") {
		// remove invalid lines
		for _, match := range numberMatch.FindAllStringSubmatch(line, -1) {
			number := utils.MustInt(match[1])
			invalid := true

		check:
			for label := range definitions {
				for _, definition := range definitions[label] {
					if number >= definition[0] && number <= definition[1] {
						invalid = false
						break check
					}

				}
			}

			if invalid {
				continue lines
			}
		}

		// collect invalid positions per label
		for pos, match := range numberMatch.FindAllStringSubmatch(line, -1) {
			number := utils.MustInt(match[1])

			for label := range definitions {
				if _, set := invalidPositions[label][pos]; !set {
					invalid := true

					for _, definition := range definitions[label] {
						if number >= definition[0] && number <= definition[1] {
							invalid = false
							break
						}
					}

					if invalid {
						invalidPositions[label][pos] = struct{}{}
					}
				}
			}
		}
	}

	positionsWithPrefix := make(map[int]struct{})
	// luckily one label is only definitions in exactly one position, one in two, one in three...
	exactlyOne := len(definitions) - 1

	for {
		fixedOne := false

		for label := range invalidPositions {
			if len(invalidPositions[label]) == exactlyOne {
				for pos := 0; pos <= exactlyOne; pos++ {
					if _, set := invalidPositions[label][pos]; !set {
						// invalidate location for all others
						for other := range invalidPositions {
							invalidPositions[other][pos] = struct{}{}
						}

						fixedOne = true
						if strings.HasPrefix(label, prefix) {
							positionsWithPrefix[pos] = struct{}{}
						}
						break
					}
				}
			}
		}

		if !fixedOne {
			break
		}
	}

	result := 1
	ownTicket := numberMatch.FindAllStringSubmatch(blocks[1], -1)

	for pos := range positionsWithPrefix {
		result *= utils.MustInt(ownTicket[pos][1])
	}

	return result
}
