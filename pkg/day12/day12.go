package day12

import (
	"advent-of-code-2020/pkg/utils"
)

func EvasiveAction(input []string) int {
	var pos utils.Coordinate2
	heading := utils.NewMovement2(utils.East, utils.South, utils.West, utils.North)

	for i := range input {
		action := input[i][0]
		value := utils.MustInt(input[i][1:])

		switch action {
		case 'N':
			pos = pos.Move(utils.Coordinate2{X: utils.North.X * value, Y: utils.North.Y * value})
		case 'S':
			pos = pos.Move(utils.Coordinate2{X: utils.South.X * value, Y: utils.South.Y * value})
		case 'E':
			pos = pos.Move(utils.Coordinate2{X: utils.East.X * value, Y: utils.East.Y * value})
		case 'W':
			pos = pos.Move(utils.Coordinate2{X: utils.West.X * value, Y: utils.West.Y * value})
		case 'L':
			heading = heading.Move(-value / 90)
		case 'R':
			heading = heading.Move(value / 90)
		case 'F':
			pos = pos.Move(utils.Coordinate2{
				X: heading.Value.(utils.Coordinate2).X * value,
				Y: heading.Value.(utils.Coordinate2).Y * value,
			})
		}
	}

	return pos.Distance(utils.Coordinate2{})
}

func FollowWaypoint(input []string) int {
	var pos utils.Coordinate2
	waypoint := utils.Coordinate2{X: 10, Y: -1}

	for i := range input {
		action := input[i][0]
		value := utils.MustInt(input[i][1:])

		switch action {
		case 'N':
			waypoint = waypoint.Move(utils.Coordinate2{X: utils.North.X * value, Y: utils.North.Y * value})
		case 'S':
			waypoint = waypoint.Move(utils.Coordinate2{X: utils.South.X * value, Y: utils.South.Y * value})
		case 'E':
			waypoint = waypoint.Move(utils.Coordinate2{X: utils.East.X * value, Y: utils.East.Y * value})
		case 'W':
			waypoint = waypoint.Move(utils.Coordinate2{X: utils.West.X * value, Y: utils.West.Y * value})
		case 'L':
			for i := 0; i < value/90; i++ {
				waypoint = utils.Coordinate2{X: waypoint.Y, Y: -waypoint.X}
			}
		case 'R':
			for i := 0; i < value/90; i++ {
				waypoint = utils.Coordinate2{X: -waypoint.Y, Y: waypoint.X}
			}
		case 'F':
			pos = pos.Move(utils.Coordinate2{X: waypoint.X * value, Y: waypoint.Y * value})
		}
	}

	return pos.Distance(utils.Coordinate2{})
}
