package day12_test

import (
	"testing"

	"advent-of-code-2020/pkg/day12"
)

func TestEvasiveAction(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"F10",
			"N3",
			"F7",
			"R90",
			"F11",
		}, 25},
		{[]string{
			"N10",
			"E10",
			"S20",
			"W10",
			"L90",
			"F10",
		}, 0},
	}

	for _, test := range tests {
		actual := day12.EvasiveAction(test.in)
		if actual != test.out {
			t.Errorf("EvasiveAction(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestFollowWaypoint(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"F10",
			"N3",
			"F7",
			"R90",
			"F11",
		}, 286},
		{[]string{
			"L360",
			"S1",
			"W10",
			"F100",
			"E10",
			"W20",
			"F10",
		},
			100},
	}

	for _, test := range tests {
		actual := day12.FollowWaypoint(test.in)
		if actual != test.out {
			t.Errorf("FollowWaypoint(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
