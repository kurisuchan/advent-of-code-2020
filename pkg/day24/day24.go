package day24

import "advent-of-code-2020/pkg/utils"

type Grid map[utils.Coordinate2]bool

func (g Grid) Neighbors(c utils.Coordinate2) int {
	var count int

	for _, dir := range utils.Coordinate2NeighborsHex {
		if g[c.Move(dir)] {
			count++
		}
	}

	return count
}

func (g Grid) Flip(c utils.Coordinate2) bool {
	n := g.Neighbors(c)

	if g[c] {
		return !(n == 0 || n > 2)
	} else {
		return n == 2
	}
}

func (g Grid) Count() int {
	var count int

	for _, v := range g {
		if v {
			count++
		}
	}

	return count
}

func MakeGrid(input []string) Grid {
	grid := make(Grid)

	for _, line := range input {
		var pos utils.Coordinate2

		for i := 0; i < len(line); i++ {
			var cur, next byte

			cur = line[i]

			if i+1 < len(line) {
				next = line[i+1]
			}

			switch {
			case cur == 's' && next == 'e':
				pos = pos.Move(utils.HexSouthEast)
				i++
			case cur == 's' && next == 'w':
				pos = pos.Move(utils.HexSouthWest)
				i++
			case cur == 'n' && next == 'e':
				pos = pos.Move(utils.HexNorthEast)
				i++
			case cur == 'n' && next == 'w':
				pos = pos.Move(utils.HexNorthWest)
				i++
			case cur == 'e':
				pos = pos.Move(utils.HexEast)
			case cur == 'w':
				pos = pos.Move(utils.HexWest)
			}
		}

		grid[pos] = !grid[pos]
	}

	return grid
}

func TileFlipper(input []string) int {
	return MakeGrid(input).Count()
}

func ArtExhibit(input []string) int {
	grid := MakeGrid(input)

	for i := 0; i < 100; i++ {
		newGrid := make(Grid)

		for pos := range grid {
			newGrid[pos] = grid.Flip(pos)

			for _, dir := range utils.Coordinate2NeighborsHex {
				np := pos.Move(dir)

				if _, ok := grid[np]; !ok {
					if !newGrid[np] && grid.Flip(np) {
						newGrid[np] = true
					}
				}
			}
		}

		grid = newGrid
	}

	return grid.Count()
}
