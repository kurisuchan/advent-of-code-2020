package day24_test

import (
	"testing"

	"advent-of-code-2020/pkg/day24"
)

func TestTileFlipper(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"sesenwnenenewseeswwswswwnenewsewsw",
			"neeenesenwnwwswnenewnwwsewnenwseswesw",
			"seswneswswsenwwnwse",
			"nwnwneseeswswnenewneswwnewseswneseene",
			"swweswneswnenwsewnwneneseenw",
			"eesenwseswswnenwswnwnwsewwnwsene",
			"sewnenenenesenwsewnenwwwse",
			"wenwwweseeeweswwwnwwe",
			"wsweesenenewnwwnwsenewsenwwsesesenwne",
			"neeswseenwwswnwswswnw",
			"nenwswwsewswnenenewsenwsenwnesesenew",
			"enewnwewneswsewnwswenweswnenwsenwsw",
			"sweneswneswneneenwnewenewwneswswnese",
			"swwesenesewenwneswnwwneseswwne",
			"enesenwswwswneneswsenwnewswseenwsese",
			"wnwnesenesenenwwnenwsewesewsesesew",
			"nenewswnwewswnenesenwnesewesw",
			"eneswnwswnwsenenwnwnwwseeswneewsenese",
			"neswnwewnwnwseenwseesewsenwsweewe",
			"wseweeenwnesenwwwswnew",
		}, 10},
	}

	for i, test := range tests {
		actual := day24.TileFlipper(test.in)
		if actual != test.out {
			t.Errorf("TileFlipper(%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestArtExhibit(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"sesenwnenenewseeswwswswwnenewsewsw",
			"neeenesenwnwwswnenewnwwsewnenwseswesw",
			"seswneswswsenwwnwse",
			"nwnwneseeswswnenewneswwnewseswneseene",
			"swweswneswnenwsewnwneneseenw",
			"eesenwseswswnenwswnwnwsewwnwsene",
			"sewnenenenesenwsewnenwwwse",
			"wenwwweseeeweswwwnwwe",
			"wsweesenenewnwwnwsenewsenwwsesesenwne",
			"neeswseenwwswnwswswnw",
			"nenwswwsewswnenenewsenwsenwnesesenew",
			"enewnwewneswsewnwswenweswnenwsenwsw",
			"sweneswneswneneenwnewenewwneswswnese",
			"swwesenesewenwneswnwwneseswwne",
			"enesenwswwswneneswsenwnewswseenwsese",
			"wnwnesenesenenwwnenwsewesewsesesew",
			"nenewswnwewswnenesenwnesewesw",
			"eneswnwswnwsenenwnwnwwseeswneewsenese",
			"neswnwewnwnwseenwseesewsenwsweewe",
			"wseweeenwnesenwwwswnew",
		}, 2208},
	}

	for i, test := range tests {
		actual := day24.ArtExhibit(test.in)
		if actual != test.out {
			t.Errorf("ArtExhibit(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
