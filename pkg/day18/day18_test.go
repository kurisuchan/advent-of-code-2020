package day18_test

import (
	"testing"

	"advent-of-code-2020/pkg/day18"
)

func TestLTRMath(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{"1 + 2 * 3 + 4 * 5 + 6"}, 71},
		{[]string{"1 + (2 * 3) + (4 * (5 + 6))"}, 51},
		{[]string{"2 * 3 + (4 * 5)"}, 26},
		{[]string{"5 + (8 * 3 + 9 + 3 * 4 * 3)"}, 437},
		{[]string{"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"}, 12240},
		{[]string{"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"}, 13632},
	}

	for _, test := range tests {
		actual := day18.LTRMath(test.in)
		if actual != test.out {
			t.Errorf("LTRMath(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAdvancedMath(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{"1 + 2 * 3 + 4 * 5 + 6"}, 231},
		{[]string{"1 + (2 * 3) + (4 * (5 + 6))"}, 51},
		{[]string{"2 * 3 + (4 * 5)"}, 46},
		{[]string{"5 + (8 * 3 + 9 + 3 * 4 * 3)"}, 1445},
		{[]string{"5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"}, 669060},
		{[]string{"((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"}, 23340},
	}

	for _, test := range tests {
		actual := day18.AdvancedMath(test.in)
		if actual != test.out {
			t.Errorf("AdvancedMath(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
