package day18

import (
	"fmt"
	"regexp"

	"advent-of-code-2020/pkg/utils"
)

type RPN []interface{}
type Token int
type Stack []Token

const (
	ILLEGAL Token = iota
	LPAREN        // (
	RPAREN        // )
	ADD           // +
	MUL           // *
)

var operators = map[string]Token{
	"+": ADD,
	"*": MUL,
}

func (s *Stack) Pop() Token {
	old := *s
	n := len(old)
	out := old[n-1]
	*s = old[:n-1]
	return out
}

func (s *Stack) Push(v Token) {
	*s = append(*s, v)
}

func (s Stack) Peek() Token {
	return s[len(s)-1]
}

func Parse(input string, precedence map[Token]int) RPN {
	tokens := regexp.MustCompile(`(\d+)|([^\d ])`)
	fields := tokens.FindAllStringSubmatch(input, -1)

	operatorStack := new(Stack)
	var output []interface{}

	if precedence == nil {
		precedence = make(map[Token]int)
	}

	for _, field := range fields {
		if field[1] != "" {
			output = append(output, utils.MustInt(field[1]))

		} else if field[2] != "" {
			switch token := field[2]; token {
			case ")":
				for {
					if len(*operatorStack) > 0 {
						if operatorStack.Peek() != LPAREN {
							output = append(output, operatorStack.Pop())
						} else {
							break
						}
					} else {
						panic(`unbalanced parentheses, missing "("`)
					}
				}

				if operatorStack.Peek() == LPAREN {
					operatorStack.Pop()
				}

			case "(":
				operatorStack.Push(LPAREN)

			case "+", "*":
				for len(*operatorStack) > 0 && operatorStack.Peek() != LPAREN &&
					precedence[operatorStack.Peek()] >= precedence[operators[token]] {

					output = append(output, operatorStack.Pop())
				}

				operatorStack.Push(operators[token])

			default:
				panic(fmt.Errorf("invalid token: %s", token))
			}
		} else {
			panic(fmt.Errorf("parser error: %q", fields))
		}
	}

	for len(*operatorStack) > 0 {
		if operatorStack.Peek() == LPAREN {
			panic(`unbalanced parentheses, missing ")"`)
		}
		output = append(output, operatorStack.Pop())
	}

	return output
}

func (s RPN) Eval() int {
	numbers := make([]int, 0)

	for _, token := range s {
		switch t := token.(type) {
		case Token:
			if len(numbers) < 2 {
				panic("missing operand")
			}

			arg1, arg2 := numbers[len(numbers)-2], numbers[len(numbers)-1]
			numbers = numbers[:len(numbers)-2]

			switch t {
			case ADD:
				numbers = append(numbers, arg1+arg2)
			case MUL:
				numbers = append(numbers, arg1*arg2)
			default:
				panic(fmt.Errorf("illegal operation: %d", t))
			}

		case int:
			numbers = append(numbers, t)
		}
	}

	if len(numbers) != 1 {
		panic("broken math")
	}

	return numbers[0]
}
