package day18

func LTRMath(input []string) int {
	var sum int

	for i := range input {
		sum += Parse(input[i], nil).Eval()
	}

	return sum
}

func AdvancedMath(input []string) int {
	var sum int
	precedence := map[Token]int{
		ADD: 1,
	}

	for i := range input {
		sum += Parse(input[i], precedence).Eval()
	}

	return sum
}
