package day07_test

import (
	"testing"

	"advent-of-code-2020/pkg/day07"
)

func TestGetBags(t *testing.T) {
	in := []string{
		"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags.",
		"",
	}
	bags := day07.GetBags(in)
	tests := map[string]bool{
		"light red bags contain 1 bright white bag":     bags["light red"].Bags[bags["bright white"]] == 1,
		"light red bags contain 2 muted yellow bags":    bags["light red"].Bags[bags["muted yellow"]] == 2,
		"dark orange bags contain 3 bright white bags":  bags["dark orange"].Bags[bags["bright white"]] == 3,
		"dark orange bags contain 4 muted yellow bags":  bags["dark orange"].Bags[bags["muted yellow"]] == 4,
		"bright white bags contain 1 shiny gold bag":    bags["bright white"].Bags[bags["shiny gold"]] == 1,
		"muted yellow bags contain 2 shiny gold bags":   bags["muted yellow"].Bags[bags["shiny gold"]] == 2,
		"muted yellow bags contain 9 faded blue bags":   bags["muted yellow"].Bags[bags["faded blue"]] == 9,
		"shiny gold bags contain 1 dark olive bag":      bags["shiny gold"].Bags[bags["dark olive"]] == 1,
		"shiny gold bags contain 2 vibrant plum bags":   bags["shiny gold"].Bags[bags["vibrant plum"]] == 2,
		"dark olive bags contain 3 faded blue bags":     bags["dark olive"].Bags[bags["faded blue"]] == 3,
		"dark olive bags contain 4 dotted black bags":   bags["dark olive"].Bags[bags["dotted black"]] == 4,
		"vibrant plum bags contain 5 faded blue bags":   bags["vibrant plum"].Bags[bags["faded blue"]] == 5,
		"vibrant plum bags contain 6 dotted black bags": bags["vibrant plum"].Bags[bags["dotted black"]] == 6,
		"light red bags contain 2 types of bag":         len(bags["light red"].Bags) == 2,
		"dark orange bags contain 2 types of bag":       len(bags["dark orange"].Bags) == 2,
		"bright white bags contain 1 type of bag":       len(bags["bright white"].Bags) == 1,
		"muted yellow bags contain 2 types of bag":      len(bags["muted yellow"].Bags) == 2,
		"shiny gold bags contain 2 types of bag":        len(bags["shiny gold"].Bags) == 2,
		"dark olive bags contain 2 types of bag":        len(bags["dark olive"].Bags) == 2,
		"vibrant plum bags contain 2 types of bag":      len(bags["vibrant plum"].Bags) == 2,
		"faded blue bags contain no other bags":         len(bags["faded blue"].Bags) == 0,
		"dotted black bags contain no other bags":       len(bags["dotted black"].Bags) == 0,
		"there are 9 types of bags":                     len(bags) == 9,
	}

	for name, test := range tests {
		if !test {
			t.Errorf("GetBags(%s) => %t, want %t", name, test, !test)
		}
	}
}

func TestWhoCanGold(t *testing.T) {
	tests := []struct {
		in  day07.Luggage
		out int
	}{
		{day07.GetBags([]string{
			"light red bags contain 1 bright white bag, 2 muted yellow bags.",
			"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
			"bright white bags contain 1 shiny gold bag.",
			"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
			"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
			"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
			"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
			"faded blue bags contain no other bags.",
			"dotted black bags contain no other bags.",
		}), 4},
	}

	for i, test := range tests {
		actual := day07.WhoCanGold(test.in)
		if actual != test.out {
			t.Errorf("WhoCanGold(%d) => %d, want %d", i, actual, test.out)
		}
	}
}

func TestHowManyBags(t *testing.T) {
	tests := []struct {
		in  day07.Luggage
		out int
	}{
		{day07.GetBags([]string{
			"light red bags contain 1 bright white bag, 2 muted yellow bags.",
			"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
			"bright white bags contain 1 shiny gold bag.",
			"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
			"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
			"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
			"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
			"faded blue bags contain no other bags.",
			"dotted black bags contain no other bags.",
		}), 32},
		{day07.GetBags([]string{
			"shiny gold bags contain 2 dark red bags.",
			"dark red bags contain 2 dark orange bags.",
			"dark orange bags contain 2 dark yellow bags.",
			"dark yellow bags contain 2 dark green bags.",
			"dark green bags contain 2 dark blue bags.",
			"dark blue bags contain 2 dark violet bags.",
			"dark violet bags contain no other bags.",
		}), 126},
	}

	for i, test := range tests {
		actual := day07.HowManyBags(test.in)
		if actual != test.out {
			t.Errorf("HowManyBags(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
