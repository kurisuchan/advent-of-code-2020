package day07

import (
	"regexp"

	"advent-of-code-2020/pkg/utils"
)

const shinyGold = "shiny gold"

type Luggage map[string]*Bag

type Bag struct {
	Bags map[*Bag]int
}

func NewBag() *Bag {
	return &Bag{
		Bags: make(map[*Bag]int),
	}
}

func (b Bag) CanHold(target *Bag) bool {
	for bag := range b.Bags {
		if bag == target {
			return true
		}

		if bag.CanHold(target) {
			return true
		}
	}

	return false
}

func (b Bag) SubBags() int {
	var sum int

	for bag, count := range b.Bags {
		sum += count + count*bag.SubBags()
	}

	return sum
}

var (
	inputPattern = regexp.MustCompile(`^(.*?) bags contain (.*)$`)
	bagPattern   = regexp.MustCompile(`(?:(\d+) (.*?) bags?)+`)
)

func GetBags(rules []string) map[string]*Bag {
	bags := map[string]*Bag{}

	for i := range rules {
		if rules[i] == "" {
			continue
		}

		bagMatch := inputPattern.FindStringSubmatch(rules[i])
		bagName := bagMatch[1]

		if _, ok := bags[bagName]; !ok {
			bags[bagName] = NewBag()
		}

		for _, m := range bagPattern.FindAllStringSubmatch(bagMatch[2], -1) {
			subBagCount := utils.MustInt(m[1])
			subBagName := m[2]

			if b, ok := bags[subBagName]; ok {
				bags[bagName].Bags[b] = subBagCount
			} else {
				subBag := NewBag()
				bags[subBagName] = subBag
				bags[bagName].Bags[subBag] = subBagCount
			}
		}
	}

	return bags
}

func WhoCanGold(bags Luggage) int {
	var count int

	for bag := range bags {
		if bags[bag].CanHold(bags[shinyGold]) {
			count++
		}
	}

	return count
}

func HowManyBags(bags Luggage) int {
	return bags[shinyGold].SubBags()
}
