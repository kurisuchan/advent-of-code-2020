package day21_test

import (
	"testing"

	"advent-of-code-2020/pkg/day21"
)

func TestSafeIngredients(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
			"trh fvjkl sbzzf mxmxvkd (contains dairy)",
			"sqjhc fvjkl (contains soy)",
			"sqjhc mxmxvkd sbzzf (contains fish)",
		}, 5},
	}

	for _, test := range tests {
		actual := day21.SafeIngredients(test.in)
		if actual != test.out {
			t.Errorf("SafeIngredients(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCanonicalDangerousIngredientList(t *testing.T) {
	tests := []struct {
		in  []string
		out string
	}{
		{[]string{
			"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
			"trh fvjkl sbzzf mxmxvkd (contains dairy)",
			"sqjhc fvjkl (contains soy)",
			"sqjhc mxmxvkd sbzzf (contains fish)",
		}, "mxmxvkd,sqjhc,fvjkl"},
		{[]string{
			"sqjhc mxmxvkd sbzzf (contains fish)",
			"sqjhc fvjkl (contains soy)",
			"trh fvjkl sbzzf mxmxvkd (contains dairy)",
			"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
		}, "mxmxvkd,sqjhc,fvjkl"},
	}

	for _, test := range tests {
		actual := day21.CanonicalDangerousIngredientList(test.in)
		if actual != test.out {
			t.Errorf("CanonicalDangerousIngredientList(%q) => %s, want %s", test.in, actual, test.out)
		}
	}
}
