package day21

import (
	"sort"
	"strings"
	"unicode"

	"advent-of-code-2020/pkg/utils"
)

func splitter(r rune) bool {
	return !unicode.IsLetter(r)
}

func parse(input []string) (map[string]int, map[string]string) {
	allIngredients := make(map[string]int)
	allergens := make(map[string]utils.Set)

	for i := range input {
		var allergen bool
		var ingredients []string
		var ingredientSet utils.Set

		for _, field := range strings.FieldsFunc(input[i], splitter) {
			if field == "contains" {
				allergen = true
				ingredientSet = utils.NewSet(ingredients)
				continue
			}

			if allergen {
				if _, ok := allergens[field]; ok {
					allergens[field] = allergens[field].Intersect(ingredientSet)
				} else {
					allergens[field] = ingredientSet
				}
			} else {
				ingredients = append(ingredients, field)
				allIngredients[field]++
			}
		}
	}

	// clean up allergens
	for {
		var cleanupNeeded bool

		for allergen := range allergens {
			if len(allergens[allergen]) == 1 {
				for ingredient := range allergens[allergen] {
					for other := range allergens {
						if other != allergen {
							delete(allergens[other], ingredient)
						}
					}
				}
			} else {
				cleanupNeeded = true
			}
		}

		if !cleanupNeeded {
			break
		}
	}

	// each allergen is found in only one ingredient, convert from map[allergen]map[ingredient] to map[allergen]ingredient
	allAllergens := make(map[string]string)
	for allergen := range allergens {
		for ingredient := range allergens[allergen] {
			allAllergens[allergen] = ingredient
		}
	}

	return allIngredients, allAllergens
}

func SafeIngredients(input []string) int {
	ingredients, allergens := parse(input)

	for allergen := range allergens {
		delete(ingredients, allergens[allergen])
	}

	var sum int

	for ingredient := range ingredients {
		sum += ingredients[ingredient]
	}

	return sum
}

func CanonicalDangerousIngredientList(input []string) string {
	_, allergenIngredients := parse(input)
	var ingredients, allergens []string

	for allergen := range allergenIngredients {
		allergens = append(allergens, allergen)
	}

	sort.Strings(allergens)

	for _, allergen := range allergens {
		ingredients = append(ingredients, allergenIngredients[allergen])
	}

	return strings.Join(ingredients, ",")
}
