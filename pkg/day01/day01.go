package day01

func ExpenseReport(expenses []int) int {
	for i := 0; i < len(expenses); i++ {
		for j := i+1; j <len(expenses); j++ {
			if expenses[i]+expenses[j] == 2020 {
				return expenses[i] * expenses[j]
			}
		}
	}
	return 0
}

func TripleExpenseReport(expenses []int) int {
	for i := 0; i < len(expenses); i++ {
		for j := i+1; j < len(expenses); j++ {
			if expenses[i]+expenses[j] >= 2020 {
				continue
			}

			for k := j+1; k < len(expenses); k++ {
				if expenses[i]+expenses[j]+expenses[k] == 2020 {
					return expenses[i] * expenses[j] * expenses[k]
				}
			}
		}
	}
	return 0
}

