package day01_test

import (
	"testing"

	"advent-of-code-2020/pkg/day01"
)

func TestExpenseReport(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			1721,
			979,
			366,
			299,
			675,
			1456,
		}, 514579},
	}

	for _, test := range tests {
		actual := day01.ExpenseReport(test.in)
		if actual != test.out {
			t.Errorf("ExpenseReport(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTripleExpenseReport(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{
			1721,
			979,
			366,
			299,
			675,
			1456,
		}, 241861950},
	}

	for _, test := range tests {
		actual := day01.TripleExpenseReport(test.in)
		if actual != test.out {
			t.Errorf("TripleExpenseReport(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
