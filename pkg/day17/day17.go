package day17

import (
	"advent-of-code-2020/pkg/utils"
)

type void = struct{}

type grid map[utils.Coordinate3]void

func (g grid) ActiveNeighbors(pos utils.Coordinate3) int {
	var active int

	for i := range utils.Coordinate3Neighbors26 {
		if _, ok := g[pos.Move(utils.Coordinate3Neighbors26[i])]; ok {
			active++
			if active > 3 {
				break
			}
		}
	}

	return active
}

func PocketCube(input []string) int {
	active := make(grid)

	for y := range input {
		for x := range input[y] {
			if input[y][x] == '#' {
				active[utils.Coordinate3{X: x, Y: y, Z: 0}] = void{}
			}
		}
	}

	for i := 0; i < 6; i++ {
		newActive := make(grid)
		visited := make(grid)

		for pos := range active {
			if n := active.ActiveNeighbors(pos); n == 2 || n == 3 {
				newActive[pos] = void{}
			}

			for y := range utils.Coordinate3Neighbors26 {
				inactive := pos.Move(utils.Coordinate3Neighbors26[y])

				if _, ok := visited[inactive]; ok {
					continue
				}

				visited[inactive] = void{}

				if _, ok := active[inactive]; !ok {
					if active.ActiveNeighbors(inactive) == 3 {
						newActive[inactive] = void{}
					}
				}
			}
		}

		active = newActive
	}

	return len(active)
}

type hyperGrid map[utils.Coordinate4]void

func (g hyperGrid) ActiveNeighbors(pos utils.Coordinate4) int {
	var active int

	for i := range utils.Coordinate4Neighbors80 {
		if _, ok := g[pos.Move(utils.Coordinate4Neighbors80[i])]; ok {
			active++
			if active > 3 {
				break
			}
		}
	}

	return active
}

func PocketHypercube(input []string) int {
	active := make(hyperGrid)

	for y := range input {
		for x := range input[y] {
			if input[y][x] == '#' {
				active[utils.Coordinate4{X: x, Y: y, Z: 0, W: 0}] = void{}
			}
		}
	}

	for i := 0; i < 6; i++ {
		newActive := make(hyperGrid)
		visited := make(hyperGrid)

		for pos := range active {
			if n := active.ActiveNeighbors(pos); n == 2 || n == 3 {
				newActive[pos] = void{}
			}

			for y := range utils.Coordinate4Neighbors80 {
				inactive := pos.Move(utils.Coordinate4Neighbors80[y])

				if _, ok := visited[inactive]; ok {
					continue
				}

				visited[inactive] = void{}

				if _, ok := active[inactive]; !ok {
					if active.ActiveNeighbors(inactive) == 3 {
						newActive[inactive] = void{}
					}
				}
			}
		}

		active = newActive
	}

	return len(active)
}
