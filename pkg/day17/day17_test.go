package day17_test

import (
	"testing"

	"advent-of-code-2020/pkg/day17"
)

func TestPocketCube(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			".#.",
			"..#",
			"###",
		}, 112},
	}

	for _, test := range tests {
		actual := day17.PocketCube(test.in)
		if actual != test.out {
			t.Errorf("PocketCube(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestPocketHypercube(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			".#.",
			"..#",
			"###",
		}, 848},
	}

	for _, test := range tests {
		actual := day17.PocketHypercube(test.in)
		if actual != test.out {
			t.Errorf("PocketHypercube(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
