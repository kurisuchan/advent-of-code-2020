package utils

type Set map[string]struct{}

func (s Set) Intersect(o Set) Set {
	intersection := make(Set)
	for k := range s {
		if _, ok := o[k]; ok {
			intersection[k] = struct{}{}
		}
	}
	return intersection
}

func (s Set) Add(i string) {
	s[i] = struct{}{}
}

func NewSet(s []string) Set {
	o := make(Set)

	for i := range s {
		o.Add(s[i])
	}

	return o
}
