package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func ReadStringFromFile(filename string) string {
	inputBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening input file: %s", err.Error())
		os.Exit(66)
	}
	return string(inputBytes)
}

func ReadStringLinesFromFile(filename string) []string {
	return strings.Split(ReadStringFromFile(filename), "\n")
}

func ReadStringSpaceGroupsFromFile(filename string) [][]string {
	var out [][]string
	for _, group := range strings.Split(ReadStringFromFile(filename), "\n\n") {
		out = append(out, strings.Split(strings.Trim(group, "\n"), "\n"))
	}

	return out
}

func ReadIntLinesFromFile(filename string) []int {
	var out []int
	for _, line := range ReadStringLinesFromFile(filename) {
		if line == "" {
			continue
		}
		num, err := strconv.Atoi(line)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error parsing int line: %s", err.Error())
			os.Exit(66)
		}
		out = append(out, num)

	}
	return out
}

func MustInt(input string) int {
	i, err := strconv.Atoi(input)
	if err != nil {
		panic(fmt.Errorf("failed to cast %s to int: %s", input, err.Error()))
	}
	return i
}

func MustUint64(input string) uint64 {
	i, err := strconv.ParseUint(input, 10, 64)
	if err != nil {
		panic(fmt.Errorf("failed to cast %s to uint64: %s", input, err.Error()))
	}
	return i
}

func NoEmptyStrings(input []string) []string {
	var i int
	for _, line := range input {
		l := strings.TrimSpace(line)
		if l != "" {
			input[i] = l
			i++
		}
	}
	return input[:i]
}
