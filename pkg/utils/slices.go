package utils

func RotateBool(image [][]bool) [][]bool {
	size := len(image)
	newImage := make([][]bool, size)

	for y := 0; y < size; y++ {
		newImage[y] = make([]bool, size)
	}

	for y := range image {
		for x := range image[y] {
			newImage[size-x-1][y] = image[y][x]
		}
	}

	return newImage
}

func FlipBool(image [][]bool) [][]bool {
	size := len(image)
	newImage := make([][]bool, size)

	for i := 0; i < size; i++ {
		newImage[i] = make([]bool, size)
	}

	for y := range image {
		for x := range image[y] {
			newImage[y][size-x-1] = image[y][x]
		}
	}

	return newImage
}

