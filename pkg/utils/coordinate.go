package utils

import "container/ring"

type Coordinate2 struct {
	X int
	Y int
}

type Coordinate3 struct {
	X int
	Y int
	Z int
}

type Coordinate4 struct {
	X int
	Y int
	Z int
	W int
}

var (
	East  = Coordinate2{X: 1, Y: 0}
	South = Coordinate2{X: 0, Y: 1}
	West  = Coordinate2{X: -1, Y: 0}
	North = Coordinate2{X: 0, Y: -1}

	NorthEast = Coordinate2{X: 1, Y: -1}
	NorthWest = Coordinate2{X: -1, Y: -1}
	SouthEast = Coordinate2{X: 1, Y: 1}
	SouthWest = Coordinate2{X: -1, Y: 1}

	HexEast      = Coordinate2{X: 1, Y: 0}
	HexSouthEast = Coordinate2{X: 0, Y: 1}
	HexSouthWest = Coordinate2{X: -1, Y: 1}
	HexWest      = Coordinate2{X: -1, Y: 0}
	HexNorthWest = Coordinate2{X: 0, Y: -1}
	HexNorthEast = Coordinate2{X: 1, Y: -1}
)

var Coordinate2Neighbors4 = [4]Coordinate2{
	North,
	East,
	South,
	West,
}

var Coordinate2Neighbors8 = [8]Coordinate2{
	North,
	NorthEast,
	East,
	SouthEast,
	South,
	SouthWest,
	West,
	NorthWest,
}

var Coordinate2NeighborsHex = [6]Coordinate2{
	HexEast,
	HexSouthEast,
	HexSouthWest,
	HexWest,
	HexNorthWest,
	HexNorthEast,
}

var Coordinate3Neighbors26 = [26]Coordinate3{
	{X: -1, Y: -1, Z: -1},
	{X: 0, Y: -1, Z: -1},
	{X: 1, Y: -1, Z: -1},
	{X: -1, Y: 0, Z: -1},
	{X: 0, Y: 0, Z: -1},
	{X: 1, Y: 0, Z: -1},
	{X: -1, Y: 1, Z: -1},
	{X: 0, Y: 1, Z: -1},
	{X: 1, Y: 1, Z: -1},
	{X: -1, Y: -1, Z: 0},
	{X: 0, Y: -1, Z: 0},
	{X: 1, Y: -1, Z: 0},
	{X: -1, Y: 0, Z: 0},
	{X: 1, Y: 0, Z: 0},
	{X: -1, Y: 1, Z: 0},
	{X: 0, Y: 1, Z: 0},
	{X: 1, Y: 1, Z: 0},
	{X: -1, Y: -1, Z: 1},
	{X: 0, Y: -1, Z: 1},
	{X: 1, Y: -1, Z: 1},
	{X: -1, Y: 0, Z: 1},
	{X: 0, Y: 0, Z: 1},
	{X: 1, Y: 0, Z: 1},
	{X: -1, Y: 1, Z: 1},
	{X: 0, Y: 1, Z: 1},
	{X: 1, Y: 1, Z: 1},
}

var Coordinate4Neighbors80 = [80]Coordinate4{
	{X: -1, Y: -1, Z: -1, W: -1},
	{X: -1, Y: -1, Z: -1, W: 0},
	{X: -1, Y: -1, Z: -1, W: 1},
	{X: -1, Y: -1, Z: 0, W: -1},
	{X: -1, Y: -1, Z: 0, W: 0},
	{X: -1, Y: -1, Z: 0, W: 1},
	{X: -1, Y: -1, Z: 1, W: -1},
	{X: -1, Y: -1, Z: 1, W: 0},
	{X: -1, Y: -1, Z: 1, W: 1},
	{X: -1, Y: 0, Z: -1, W: -1},
	{X: -1, Y: 0, Z: -1, W: 0},
	{X: -1, Y: 0, Z: -1, W: 1},
	{X: -1, Y: 0, Z: 0, W: -1},
	{X: -1, Y: 0, Z: 0, W: 0},
	{X: -1, Y: 0, Z: 0, W: 1},
	{X: -1, Y: 0, Z: 1, W: -1},
	{X: -1, Y: 0, Z: 1, W: 0},
	{X: -1, Y: 0, Z: 1, W: 1},
	{X: -1, Y: 1, Z: -1, W: -1},
	{X: -1, Y: 1, Z: -1, W: 0},
	{X: -1, Y: 1, Z: -1, W: 1},
	{X: -1, Y: 1, Z: 0, W: -1},
	{X: -1, Y: 1, Z: 0, W: 0},
	{X: -1, Y: 1, Z: 0, W: 1},
	{X: -1, Y: 1, Z: 1, W: -1},
	{X: -1, Y: 1, Z: 1, W: 0},
	{X: -1, Y: 1, Z: 1, W: 1},
	{X: 0, Y: -1, Z: -1, W: -1},
	{X: 0, Y: -1, Z: -1, W: 0},
	{X: 0, Y: -1, Z: -1, W: 1},
	{X: 0, Y: -1, Z: 0, W: -1},
	{X: 0, Y: -1, Z: 0, W: 0},
	{X: 0, Y: -1, Z: 0, W: 1},
	{X: 0, Y: -1, Z: 1, W: -1},
	{X: 0, Y: -1, Z: 1, W: 0},
	{X: 0, Y: -1, Z: 1, W: 1},
	{X: 0, Y: 0, Z: -1, W: -1},
	{X: 0, Y: 0, Z: -1, W: 0},
	{X: 0, Y: 0, Z: -1, W: 1},
	{X: 0, Y: 0, Z: 0, W: -1},
	{X: 0, Y: 0, Z: 0, W: 1},
	{X: 0, Y: 0, Z: 1, W: -1},
	{X: 0, Y: 0, Z: 1, W: 0},
	{X: 0, Y: 0, Z: 1, W: 1},
	{X: 0, Y: 1, Z: -1, W: -1},
	{X: 0, Y: 1, Z: -1, W: 0},
	{X: 0, Y: 1, Z: -1, W: 1},
	{X: 0, Y: 1, Z: 0, W: -1},
	{X: 0, Y: 1, Z: 0, W: 0},
	{X: 0, Y: 1, Z: 0, W: 1},
	{X: 0, Y: 1, Z: 1, W: -1},
	{X: 0, Y: 1, Z: 1, W: 0},
	{X: 0, Y: 1, Z: 1, W: 1},
	{X: 1, Y: -1, Z: -1, W: -1},
	{X: 1, Y: -1, Z: -1, W: 0},
	{X: 1, Y: -1, Z: -1, W: 1},
	{X: 1, Y: -1, Z: 0, W: -1},
	{X: 1, Y: -1, Z: 0, W: 0},
	{X: 1, Y: -1, Z: 0, W: 1},
	{X: 1, Y: -1, Z: 1, W: -1},
	{X: 1, Y: -1, Z: 1, W: 0},
	{X: 1, Y: -1, Z: 1, W: 1},
	{X: 1, Y: 0, Z: -1, W: -1},
	{X: 1, Y: 0, Z: -1, W: 0},
	{X: 1, Y: 0, Z: -1, W: 1},
	{X: 1, Y: 0, Z: 0, W: -1},
	{X: 1, Y: 0, Z: 0, W: 0},
	{X: 1, Y: 0, Z: 0, W: 1},
	{X: 1, Y: 0, Z: 1, W: -1},
	{X: 1, Y: 0, Z: 1, W: 0},
	{X: 1, Y: 0, Z: 1, W: 1},
	{X: 1, Y: 1, Z: -1, W: -1},
	{X: 1, Y: 1, Z: -1, W: 0},
	{X: 1, Y: 1, Z: -1, W: 1},
	{X: 1, Y: 1, Z: 0, W: -1},
	{X: 1, Y: 1, Z: 0, W: 0},
	{X: 1, Y: 1, Z: 0, W: 1},
	{X: 1, Y: 1, Z: 1, W: -1},
	{X: 1, Y: 1, Z: 1, W: 0},
	{X: 1, Y: 1, Z: 1, W: 1},
}

func (c Coordinate2) Move(t Coordinate2) Coordinate2 {
	return Coordinate2{
		X: c.X + t.X,
		Y: c.Y + t.Y,
	}
}

func (c Coordinate3) Move(t Coordinate3) Coordinate3 {
	return Coordinate3{
		X: c.X + t.X,
		Y: c.Y + t.Y,
		Z: c.Z + t.Z,
	}
}

func (c Coordinate4) Move(t Coordinate4) Coordinate4 {
	return Coordinate4{
		X: c.X + t.X,
		Y: c.Y + t.Y,
		Z: c.Z + t.Z,
		W: c.W + t.W,
	}
}

func NewMovement2(directions ...Coordinate2) *ring.Ring {
	r := ring.New(len(directions))

	for _, coord := range directions {
		r.Value = coord
		r = r.Next()
	}

	return r
}

func (c Coordinate2) Distance(t Coordinate2) int {
	return AbsInt(c.X-t.X) + AbsInt(c.Y-t.Y)
}
