package day11_test

import (
	"testing"

	"advent-of-code-2020/pkg/day11"
)

func TestAdjacentSeating(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"L.LL.LL.LL",
			"LLLLLLL.LL",
			"L.L.L..L..",
			"LLLL.LL.LL",
			"L.LL.LL.LL",
			"L.LLLLL.LL",
			"..L.L.....",
			"LLLLLLLLLL",
			"L.LLLLLL.L",
			"L.LLLLL.LL",
		}, 37},
	}

	for _, test := range tests {
		actual := day11.AdjacentSeating(test.in)
		if actual != test.out {
			t.Errorf("AdjacentSeating(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestVisibleSeating(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"L.LL.LL.LL",
			"LLLLLLL.LL",
			"L.L.L..L..",
			"LLLL.LL.LL",
			"L.LL.LL.LL",
			"L.LLLLL.LL",
			"..L.L.....",
			"LLLLLLLLLL",
			"L.LLLLLL.L",
			"L.LLLLL.LL",
		}, 26},
	}

	for _, test := range tests {
		actual := day11.VisibleSeating(test.in)
		if actual != test.out {
			t.Errorf("VisibleSeating(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
