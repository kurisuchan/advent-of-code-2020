package day11

import (
	"advent-of-code-2020/pkg/utils"
)

type FloorPlan map[utils.Coordinate2]bool
type VisibleCoordinates map[utils.Coordinate2][]utils.Coordinate2

type SeatMap struct {
	Plan    FloorPlan
	Visible VisibleCoordinates
}

func NewMap(input []string) SeatMap {
	plan := SeatMap{
		Plan:    make(FloorPlan),
		Visible: make(VisibleCoordinates),
	}

	var x, y int

	for y = range input {
		for x = range input[y] {
			if input[y][x] == 'L' {
				plan.Plan[utils.Coordinate2{X: x, Y: y}] = false
			}
		}
	}

	for pos := range plan.Plan {
		for _, offset := range utils.Coordinate2Neighbors8 {
			checkPos := utils.Coordinate2{X: pos.X + offset.X, Y: pos.Y + offset.Y}

			for checkPos.X >= 0 && checkPos.X <= x && checkPos.Y >= 0 && checkPos.Y <= y {
				if _, ok := plan.Plan[checkPos]; ok {
					plan.Visible[pos] = append(plan.Visible[pos], checkPos)
					break
				}

				checkPos.X += offset.X
				checkPos.Y += offset.Y
			}
		}
	}

	return plan
}

type SeatCheckFunc func(SeatMap, utils.Coordinate2) int

func OccupiedNeighbors(s SeatMap, pos utils.Coordinate2) int {
	var occupied int

	for _, offset := range utils.Coordinate2Neighbors8 {
		if s.Plan[utils.Coordinate2{X: pos.X + offset.X, Y: pos.Y + offset.Y}] {
			occupied++
		}
	}

	return occupied
}

func OccupiedVisible(s SeatMap, pos utils.Coordinate2) int {
	var occupied int

	for i := range s.Visible[pos] {
		if s.Plan[s.Visible[pos][i]] {
			occupied++
		}
	}

	return occupied
}

func (s SeatMap) TotalOccupied() int {
	var occupied int

	for pos := range s.Plan {
		if s.Plan[pos] {
			occupied++
		}
	}

	return occupied
}

func (s *SeatMap) Tick(threshold int, checkFunc SeatCheckFunc) bool {
	var changed bool

	newPlan := make(FloorPlan)

	for pos := range s.Plan {
		neighbors := checkFunc(*s, pos)

		if (!s.Plan[pos] && neighbors == 0) || (s.Plan[pos] && neighbors >= threshold) {
			changed = true
			newPlan[pos] = !s.Plan[pos]

		} else {
			newPlan[pos] = s.Plan[pos]
		}
	}

	s.Plan = newPlan

	return changed
}

func AdjacentSeating(input []string) int {
	plan := NewMap(input)

	for plan.Tick(4, OccupiedNeighbors) {
		continue
	}

	return plan.TotalOccupied()
}

func VisibleSeating(input []string) int {
	plan := NewMap(input)

	for plan.Tick(5, OccupiedVisible) {
		continue
	}

	return plan.TotalOccupied()
}
