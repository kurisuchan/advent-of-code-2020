package day14_test

import (
	"testing"

	"advent-of-code-2020/pkg/day14"
)

func TestMaskValues(t *testing.T) {
	tests := []struct {
		in  []string
		out uint64
	}{
		{[]string{
			"mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
			"mem[8] = 11",
			"mem[7] = 101",
			"mem[8] = 0",
		}, 165},
	}

	for _, test := range tests {
		actual := day14.MaskValues(test.in)
		if actual != test.out {
			t.Errorf("MaskValues(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestFloatingAddresses(t *testing.T) {
	tests := []struct {
		in  []string
		out uint64
	}{
		{[]string{
			"mask = 000000000000000000000000000000X1001X",
			"mem[42] = 100",
			"mask = 00000000000000000000000000000000X0XX",
			"mem[26] = 1",
		}, 208},
	}

	for _, test := range tests {
		actual := day14.FloatingAddresses(test.in)
		if actual != test.out {
			t.Errorf("FloatingAddresses(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
