package day14

import (
	"regexp"

	"advent-of-code-2020/pkg/utils"
)

var (
	memPattern = regexp.MustCompile(`^mem\[(\d+)] = (\d+)$`)
)

func sum(m map[uint64]uint64) uint64 {
	var total uint64

	for i := range m {
		total += m[i]
	}

	return total
}

func MaskValues(input []string) uint64 {
	memory := make(map[uint64]uint64)
	var maskSet, maskClear uint64

	for i := range input {
		switch input[i][:3] {
		case "mas":
			maskClear, maskSet = 0, 0
			mask := input[i][7:]

			for p, r := range mask {
				switch r {
				case '0':
					maskClear |= 1 << (len(mask) - p - 1)
				case '1':
					maskSet |= 1 << (len(mask) - p - 1)
				}
			}
		case "mem":
			match := memPattern.FindStringSubmatch(input[i])
			address := utils.MustUint64(match[1])
			value := utils.MustUint64(match[2])

			memory[address] = (value &^ maskClear) | maskSet
		}
	}

	return sum(memory)
}

func FloatingAddresses(input []string) uint64 {
	memory := make(map[uint64]uint64)
	var maskSet uint64
	var floating []int

	for i := range input {
		switch input[i][:3] {
		case "mas":
			maskSet = 0
			floating = nil
			mask := input[i][7:]

			for p, r := range mask {
				switch r {
				case 'X':
					floating = append(floating, len(mask)-p-1)
				case '1':
					maskSet |= 1 << (len(mask) - p - 1)
				}
			}

		case "mem":
			match := memPattern.FindStringSubmatch(input[i])
			address := utils.MustUint64(match[1])
			value := utils.MustUint64(match[2])

			addresses := make([]uint64, 1<<len(floating))
			addresses[0] = address | maskSet

			for f := range floating {
				for a := 0; a < 1<<f; a++ {
					addresses[a+(1<<f)] = addresses[a] ^ (1 << floating[f])
				}
			}

			for a := range addresses {
				memory[addresses[a]] = value
			}

		}
	}

	return sum(memory)
}
