package day22

import (
	"fmt"
	"strings"

	"advent-of-code-2020/pkg/utils"
)

type Deck []int

func (d *Deck) Pop() int {
	out := (*d)[0]
	*d = (*d)[1:]
	return out
}

func (d *Deck) Push(v ...int) {
	*d = append(*d, v...)
}

func (d *Deck) Copy(s int) *Deck {
	newDeck := new(Deck)

	for i := 0; i < s; i++ {
		*newDeck = append(*newDeck, (*d)[i])
	}

	return newDeck
}

func (d *Deck) Score() int {
	var score int
	for i := len(*d) - 1; i >= 0; i-- {
		score += (*d)[i] * (len(*d) - i)
	}
	return score
}

func (d *Deck) String() string {
	var b strings.Builder

	for i := 0; i < len(*d); i++ {
		fmt.Fprintf(&b, "%d,", (*d)[i])
	}

	return b.String()
}

func deal(input string) []*Deck {
	var decks []*Deck

	for _, block := range strings.Split(input, "\n\n") {
		deck := new(Deck)

		for _, card := range strings.Split(block, "\n")[1:] {
			if card != "" {
				deck.Push(utils.MustInt(card))
			}
		}

		decks = append(decks, deck)
	}

	if decks == nil || len(decks) != 2 || len(*decks[0]) == 0 || len(*decks[1]) == 0 {
		panic(`(╯°□°）╯︵ ┻━┻`)
	}

	return decks
}

func Combat(input string) int {
	decks := deal(input)

	for len(*decks[0]) > 0 && len(*decks[1]) > 0 {
		player1, player2 := decks[0].Pop(), decks[1].Pop()
		if player1 > player2 {
			decks[0].Push(player1, player2)
		} else {
			decks[1].Push(player2, player1)
		}
	}

	var winner int

	if len(*decks[1]) > 0 {
		winner = 1
	}

	return decks[winner].Score()
}

func recursiveGameRound(decks []*Deck) int {
	var winner int
	previous := make(map[string]struct{})

	for len(*decks[0]) > 0 && len(*decks[1]) > 0 {
		prev := (*decks[0]).String() + "🎴" + (*decks[1]).String()

		if _, ok := previous[prev]; ok {
			return 0
		}

		previous[prev] = struct{}{}

		player1, player2 := decks[0].Pop(), decks[1].Pop()

		if len(*decks[0]) >= player1 && len(*decks[1]) >= player2 {
			winner = recursiveGameRound([]*Deck{(*decks[0]).Copy(player1), (*decks[1]).Copy(player2)})
		} else {
			if player1 > player2 {
				winner = 0
			} else {
				winner = 1
			}
		}

		if winner == 0 {
			decks[0].Push(player1, player2)
		} else {
			decks[1].Push(player2, player1)
		}
	}

	if len(*decks[0]) > 0 {
		winner = 0
	} else {
		winner = 1
	}

	return winner
}

func RecursiveCombat(input string) int {
	decks := deal(input)

	return (*decks[recursiveGameRound(decks)]).Score()
}
