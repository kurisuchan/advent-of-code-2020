package day22_test

import (
	"testing"

	"advent-of-code-2020/pkg/day22"
)

func TestCombat(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`, 306},
	}

	for _, test := range tests {
		actual := day22.Combat(test.in)
		if actual != test.out {
			t.Errorf("Combat(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRecursiveCombat(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`, 291},
		{`Player 1:
43
19

Player 2:
2
29
14`, 105},
	}

	for _, test := range tests {
		actual := day22.RecursiveCombat(test.in)
		if actual != test.out {
			t.Errorf("RecursiveCombat(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
