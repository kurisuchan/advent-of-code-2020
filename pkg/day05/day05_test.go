package day05_test

import (
	"testing"

	"advent-of-code-2020/pkg/day05"
)

func TestSeatID(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"FBFBBFF", 44},
		{"RLR", 5},
		{"BFFFBBF", 70},
		{"RRR", 7},
		{"FFFBBBF", 14},
		{"BBFFBBF", 102},
		{"RLL", 4},
		{"FBFBBFFRLR", 357},
		{"BFFFBBFRRR", 567},
		{"FFFBBBFRRR", 119},
		{"BBFFBBFRLL", 820},
	}

	for _, test := range tests {
		actual := day05.SeatID(test.in)
		if actual != test.out {
			t.Errorf("SeatID(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestHighestSeatID(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"FBFBBFFRLR",
			"BFFFBBFRRR",
			"FFFBBBFRRR",
			"BBFFBBFRLL",
		}, 820},
	}

	for _, test := range tests {
		actual := day05.HighestSeatID(test.in)
		if actual != test.out {
			t.Errorf("HighestSeatID(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMissingSeatID(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"__B",
			"_B_",
			"B__",
		}, 3},
		{[]string{
			"BBBBBBBBB_",
			"BBBBBBB___",
			"BBBBBBB_BB",
			"BBBBBBBBBB",
			"BBBBBBB_B_",
			"",
			"BBBBBBB__B",
			"BBBBBBBB_B",
		}, 1020},
		{[]string{
			"__",
			"BB",
			"_B",
			"B_",
		}, -1},
	}

	for i, test := range tests {
		actual := day05.MissingSeatID(test.in)
		if actual != test.out {
			t.Errorf("MissingSeatID(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
