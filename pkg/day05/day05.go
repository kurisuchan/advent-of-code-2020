package day05

import (
	"sort"
)

func SeatID(pass string) int {
	var seat int

	for i := range pass {
		seat <<= 1
		if pass[i] == 'B' || pass[i] == 'R' {
			seat |= 1
		}
	}
	return seat
}

func HighestSeatID(passes []string) int {
	var highest int

	for i := range passes {
		id := SeatID(passes[i])
		if id > highest {
			highest = id
		}
	}

	return highest
}

func MissingSeatID(passes []string) int {
	var ids []int

	for i := range passes {
		if passes[i] == "" {
			continue
		}
		ids = append(ids, SeatID(passes[i]))
	}

	sort.Ints(ids)

	for i := 0; i < len(ids)-1; i++ {
		if ids[i]+1 != ids[i+1] {
			return ids[i] + 1
		}
	}

	return -1
}
