package day08

import (
	"strings"

	"advent-of-code-2020/pkg/utils"
)

func InfiniteLoop(input []string) int {
	var pos, acc int
	seen := make(map[int]struct{}, len(input))

	for {
		if _, ok := seen[pos]; ok {
			return acc
		} else {
			seen[pos] = struct{}{}
		}

		switch input[pos][:3] {
		case "jmp":
			pos += utils.MustInt(input[pos][4:])
		case "acc":
			acc += utils.MustInt(input[pos][4:])
			pos++
		default:
			pos++
		}
	}
}

func CleanExit(input []string) int {
	result := make(chan int, 0)
	replacer := strings.NewReplacer("jmp", "nop", "nop", "jmp")

	for i := range input {
		if input[i][:3] == "acc" {
			continue
		}

		cp := make([]string, len(input))
		copy(cp, input)
		cp[i] = replacer.Replace(cp[i])

		go isThisItMaybe(cp, result)
	}

	return <-result
}

func isThisItMaybe(input []string, result chan<- int) {
	var pos, acc int
	seen := make(map[int]struct{}, len(input))

	for {
		if _, ok := seen[pos]; ok {
			return
		} else {
			seen[pos] = struct{}{}
		}

		if pos == len(input) {
			result <- acc
			return
		} else if pos < 0 || pos >= len(input) {
			return
		}

		switch input[pos][:3] {
		case "acc":
			acc += utils.MustInt(input[pos][4:])
			pos++
		case "jmp":
			pos += utils.MustInt(input[pos][4:])
		default:
			pos++
		}
	}
}
