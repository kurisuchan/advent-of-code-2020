package day08_test

import (
	"testing"

	"advent-of-code-2020/pkg/day08"
)

func TestInfiniteLoop(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"nop +0",
			"acc +1",
			"jmp +4",
			"acc +3",
			"jmp -3",
			"acc -99",
			"acc +1",
			"jmp -4",
			"acc +6",
		}, 5},
	}

	for _, test := range tests {
		actual := day08.InfiniteLoop(test.in)
		if actual != test.out {
			t.Errorf("InfiniteLoop(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCleanExit(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"nop +0",
			"acc +1",
			"jmp +4",
			"acc +3",
			"jmp -3",
			"acc -99",
			"acc +1",
			"jmp -4",
			"acc +6",
		}, 8},
	}

	for _, test := range tests {
		actual := day08.CleanExit(test.in)
		if actual != test.out {
			t.Errorf("CleanExit(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
