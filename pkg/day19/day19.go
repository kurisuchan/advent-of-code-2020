package day19

import (
	"regexp"
	"strings"
)

// Day 19: The "I don't even care what it looks like anymore" edition

type Rule struct {
	Pattern string
	Sub     [][]*Rule
}

func parseRules(input string) *Rule {
	rules := make(map[string]*Rule)

	// get X: "X"
	for _, line := range strings.Split(input, "\n") {
		fields := strings.Fields(line)
		id := strings.TrimSuffix(fields[0], ":")
		if fields[1][0] == '"' {
			rules[id] = &Rule{Pattern: strings.Trim(fields[1], `"`)}
		} else {
			rules[id] = &Rule{}
		}
	}

	// get all the other stuff
	for _, line := range strings.Split(input, "\n") {
		fields := strings.Fields(line)
		if fields[1][0] == '"' {
			continue
		}

		id := strings.TrimSuffix(fields[0], ":")
		var r []*Rule

		for _, rule := range fields[1:] {
			if rule == "|" {
				rules[id].Sub = append(rules[id].Sub, r)
				r = nil
				continue
			}

			r = append(r, rules[rule])
		}

		rules[id].Sub = append(rules[id].Sub, r)

	}

	return rules["0"]
}

func (r *Rule) Regex() string {
	if r.Pattern != "" {
		return r.Pattern
	}

	var patterns []string

	for _, s := range r.Sub {
		var sub []string

		for _, r := range s {
			sub = append(sub, r.Regex())
		}

		patterns = append(patterns, strings.Join(sub, ""))
	}

	return "(?:" + strings.Join(patterns, "|") + ")"
}

func X(input string) int {
	blocks := strings.Split(input, "\n\n")
	pattern := parseRules(blocks[0]).Regex()
	rule := regexp.MustCompile("^" + pattern + "$")

	var valid int

	for _, message := range strings.Split(blocks[1], "\n") {
		if rule.MatchString(message) {
			valid++
		}
	}

	return valid
}

func Y(input string) int {
	blocks := strings.Split(input, "\n\n")

	// You've altered the rules
	rulebook := strings.NewReplacer(
		"8: 42",
		"8: 42 | 42 8",

		"11: 42 31",
		"11: 42 31 | 42 11 31",
	)
	blocks[0] = rulebook.Replace(blocks[0])

	// Pray **I** don't alter them even further
	hax := strings.NewReplacer(
		"8: 42 | 42 8",
		"8: 42 | 42 42 | 42 42 42 | 42 42 42 42 | 42 42 42 42 42 | 42 42 42 42 42 42 | 42 42 42 42 42 42 42 | 42 42 42 42 42 42 42 42 | 42 42 42 42 42 42 42 42 42 | 42 42 42 42 42 42 42 42 42 42",

		"11: 42 31 | 42 11 31",
		"11: 42 31 | 42 42 31 31 | 42 42 42 31 31 31 | 42 42 42 42 31 31 31 31 | 42 42 42 42 42 31 31 31 31 31 | 42 42 42 42 42 42 31 31 31 31 31 31 | 42 42 42 42 42 42 42 31 31 31 31 31 31 31",
	)
	blocks[0] = hax.Replace(blocks[0])

	pattern := parseRules(blocks[0]).Regex()
	rule := regexp.MustCompile("^" + pattern + "$")

	var valid int

	for _, message := range strings.Split(blocks[1], "\n") {
		if rule.MatchString(message) {
			valid++
		}
	}

	return valid
}
